{ inputs, config, lib, pkgs, ... }:
{
  #@# hosts/hp_pavilion/configuration.nix: main configuration for Hp Pavilion notebook
  #@#   contains:
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    inputs.home-manager.nixosModules.home-manager
    ./../../modules
  ];
  
  #@#   * hostname
  #@#   * GPU PCI ports
  # available with $ lspci -k | grep -A 2 -E "(VGA|3D)"
  networking.hostName = "sphHpNixos";
  hardware.nvidia = {
    prime = {
      intelBusId = "PCI:0:2:0";
      nvidiaBusId = "PCI:1:0:0";
    };
  };
  
  # starting with separate home.nix files
  # possibly merge later
  home-manager = {
    extraSpecialArgs = { inherit inputs; };
    users = {
      sph = import ./home.nix;
    };
  };
  
  nyx = {
  #@#   * list of modules to import with nix options
    work.enable    = true;
    sound.enable   = true;
    nvidia.enable  = true;
    games.enable   = true;
    virt.enable    = true;
    wayland.enable = true;
    zsh = {
      enable = true;
      stationID = "NIXHP";
      promptTheme = "Minimal";
      ntfyshChannel = "amogusamogus123321654456";
      s_ID = "H";
      flakePath = "~/.config/nixos"; # the only thing that should be symlinked imperatively
      flakeNixosID = "sphHpNixos";
    };
  };

}

