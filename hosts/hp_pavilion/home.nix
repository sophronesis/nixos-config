{ inputs, lib, pkgs, ... }:

{
  #@# hosts/hp_pavilion/home.nix: home-manager configuration for Hp Pavilion notebook
  #@#   contains:
  home = {
    username = "sph";
    homeDirectory = "/home/sph";
    stateVersion = "24.05";
  };
  
  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    zsh
  ];
  home.sessionVariables = {
    EDITOR = "nvim";
    TERMINAL = "kitty";
    VISUAL = "nvim";
    BROWSER = "librewolf";
    XKB_DEFAULT_LAYOUT = "us";
    NIXPKGS_ALLOW_UNFREE = "1";
    GRIM_DEFAULT_DIR = "$HOME/screencaps/";
    XDG_PICTURES_DIR = "$HOME/screencaps/";
    STEAM_EXTRA_COMPAT_TOOLS_PATHS = 
      "$HOME/.steam/root/compatibilitytools.d";
    };

  programs.git = {
    enable = true;
    userName = "sophronesis";
    userEmail = "oleksandr.buzynnyi@gmail.com";
  };
  
  # TODO add dark gtk theme
  #@#   * mimeApps associations
  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = {
    "x-scheme-handler/http" = ["firefox.desktop"];
    "x-scheme-handler/https" = ["firefox.desktop"];
    "text/html" = ["firefox.desktop"];
    "text/plain" = [ "neovide.desktop" ];
    "application/pdf" = [ "org.kde.okular.desktop" ];
    "image/*" = [ "feh.desktop" ];
    "video/*" = [ "mpv.desktop" ];
  };

  xdg.mimeApps.associations.removed = {
    "application/pdf" = ["chromium-browser.desktop"];
    "image/png" = ["chromium-browser.desktop"];
    "x-scheme-handler/http" = ["chromium-browser.desktop"];
    "x-scheme-handler/https" = ["chromium-browser.desktop"];
  };

  #@#   * .local/bin binaries
  home.file = {
    # shell binaries
    ".local/bin" = {
      source = ../../bin;
      recursive = true;
    };
  #@#   * .config dot files
    # zsh
    ".config" = {
      source = ../../config;
      recursive = true;
    };
  };
  # hyprland plugins:
  # wayland.windowManager.hyprland.plugins = [
  #   # ... whatever
  #   # inputs.Hyprspace.packages.${pkgs.system}.Hyprspace
  #   inputs.hyprland-plugins.packages.${pkgs.system}.hyprbars
  # ];
  # wayland.windowManager.hyprland.enable = true;

  # for testing
  #@#   * direnv 
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
  # programs.starship = {
  #   enable = true;
  # };
  programs.zsh.enable = true;
  programs.zsh.history.size = 1000000;
  programs.home-manager.enable = true;
}
