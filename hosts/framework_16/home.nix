{ inputs, lib, pkgs, ... }:
{
  #@# hosts/hp_pavilion/home.nix: home-manager configuration for Hp Pavilion notebook
  #@#   contains:
  # imports = [
  #   ./../../modules/hm
  # ];
  # nyx_hm.hyprland.enable = true;

  home = {
    username = "sph";
    homeDirectory = "/home/sph";
    stateVersion = "24.05";
    enableNixpkgsReleaseCheck = false;
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    zsh
    # inputs.hyprland-qtutils.packages."${pkgs.system}".default
  ];
  home.sessionVariables = {
    EDITOR = "nvim";
    TERMINAL = "kitty";
    VISUAL = "nvim";
    BROWSER = "librewolf";
    XKB_DEFAULT_LAYOUT = "us";
    NIXPKGS_ALLOW_UNFREE = "1";
    GRIM_DEFAULT_DIR = "$HOME/screencaps/";
    XDG_PICTURES_DIR = "$HOME/screencaps/";
    STEAM_EXTRA_COMPAT_TOOLS_PATHS =
      "$HOME/.steam/root/compatibilitytools.d";
  };

  programs.git = {
    enable = true;
    userName = "sophronesis";
    userEmail = "oleksandr.buzynnyi@gmail.com";
  };

  # TODO add dark gtk theme
  #@#   * mimeApps associations
  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = {
    "x-scheme-handler/http" = [ "firefox.desktop" ];
    "x-scheme-handler/https" = [ "firefox.desktop" ];
    "text/html" = [ "firefox.desktop" ];
    "text/plain" = [ "neovide.desktop" ];
    "application/pdf" = [ "org.kde.okular.desktop" ];
    "image/*" = [ "feh.desktop" ];
    "video/*" = [ "mpv.desktop" ];
  };

  xdg.mimeApps.associations.removed = {
    "application/pdf" = [ "chromium-browser.desktop" ];
    "image/png" = [ "chromium-browser.desktop" ];
    "x-scheme-handler/http" = [ "chromium-browser.desktop" ];
    "x-scheme-handler/https" = [ "chromium-browser.desktop" ];
  };

  #@#   * .local/bin binaries
  home.file = {
    # shell binaries
    ".local/bin" = {
      source = ../../bin;
      recursive = true;
    };
    #@#   * .config dot files
    # zsh
    ".config" = {
      source = ../../config;
      recursive = true;
    };
  };
  # for testing
  #@#   * direnv 
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
  programs.zsh.enable = true;
  programs.zsh.history.size = 1000000;
  programs.librewolf = {
    enable = true;
    # Enable WebGL, cookies and history
    settings = {
      "webgl.disabled" = false;
      "privacy.resistFingerprinting" = false;
      "privacy.clearOnShutdown.history" = false;
      "privacy.clearOnShutdown.cookies" = false;
      "network.cookie.lifetimePolicy" = 0;
      "general.autoScroll" = true;
      "browser.compactmode.show" = true;
      "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
      "browser.toolbars.bookmarks.visibility" = "newtab";
      "gnomeTheme.hideSingleTab" = true;
      "svg.context-properties.content.enabled" = true;
      "media.hardware-video-decoding.force-enabled" = true;
      "toolkit.tabbox.switchByScrolling" = true;
      "device.sensors.motion.enabled" = false;
    };
    # policies = {
    #   DisableTelemetry = true;
    # };
  };

  # force override of mimeapps.list file to prevent it from stucking home manager profile switch
  # https://github.com/nix-community/home-manager/issues/1213
  xdg.configFile."mimeapps.list".force = true;

  # textfox.enable = true;
  programs.home-manager.enable = true;
}
