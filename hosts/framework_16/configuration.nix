{ inputs, config, lib, pkgs, ... }:
{
  #@# hosts/framework_16/configuration.nix: main configuration for Framework 16 notebook
  #@#   contains:
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    inputs.home-manager.nixosModules.home-manager
    ./../../modules
  ];
  
  #@#   * hostname
  networking.hostName = "sphFW16Nixos";
  
  # starting with separate home.nix files
  # possibly merge later
  home-manager = {
    extraSpecialArgs = { inherit inputs; };
    users = {
      sph = import ./home.nix;
    };
  };
  
  nyx = {
  #@#   * list of modules to import with nix options
    work.enable    = true;
    sound.enable   = true;
    amdgpu.enable  = true;
    games.enable   = true;
    virt.enable    = true;
    wayland.enable = true;
    zsh = {
      enable = true;
      stationID = "NIXFW16";
      promptTheme = "Minimal";
      ntfyshChannel = "amogusamogus123321654456";
      s_ID = "F";
      s_CLR = "yellow";
      flakePath = "~/.config/nixos"; # the only thing that should be symlinked imperatively
      flakeNixosID = "sphFW16Nixos";
    };
  };
  #@#   * fans control
  security.sudo = {
    enable = true;
    extraRules = [{
      commands = [
        {
          command = "${pkgs.fw-fanctrl}/bin/fw-fanctrl";
          options = [ "NOPASSWD" ];
        }
      ];
    }];
  };
  programs.fw-fanctrl.enable = true;

  # Add a custom config
  programs.fw-fanctrl.config = {
    defaultStrategy = "lazy";
    strategies = {
      "lazy" = {
        fanSpeedUpdateFrequency = 5;
        movingAverageInterval = 30;
        speedCurve = [
          { temp =  0; speed = 15; }
          { temp = 50; speed = 15; }
          { temp = 60; speed = 25; }
          { temp = 65; speed = 30; }
          { temp = 70; speed = 40; }
          { temp = 75; speed = 50; }
          { temp = 80; speed = 70; }
          { temp = 85; speed = 100; }
        ];
      };
      "f0%" = {
        fanSpeedUpdateFrequency = 1;
        movingAverageInterval =   1;
        speedCurve = [
          { temp =  0; speed = 0; }
        ];
      };
      "f25%" = {
        fanSpeedUpdateFrequency = 1;
        movingAverageInterval =   1;
        speedCurve = [
          { temp =  0; speed = 25; }
        ];
      };
      "f50%" = {
        fanSpeedUpdateFrequency = 1;
        movingAverageInterval =   1;
        speedCurve = [
          { temp =  0; speed = 50; }
        ];
      };
      "f75%" = {
        fanSpeedUpdateFrequency = 1;
        movingAverageInterval =   1;
        speedCurve = [
          { temp =  0; speed = 75; }
        ];
      };
      "f100%" = {
        fanSpeedUpdateFrequency = 1;
        movingAverageInterval =   1;
        speedCurve = [
          { temp =  0; speed = 100; }
        ];
      };
    };
  };


  #@#   *  mullvad vpn as separate specialisation
  specialisation.mvpn.configuration.nyx.vpn.enable = true;

  #@#   * open-webui container
  system.activationScripts = {
    script.text = ''
      install -d -m 755 /home/sph/open-webui/data -o root -g root
    '';
  };

  # virtualisation = {
  #   podman = {
  #     enable = true;
  #     # dockerCompat = true;
  #     #defaultNetwork.settings.dns_enabled = true;
  #   };
  # oci-containers = {
  #   backend = "podman";
  #   containers = {
  #     open-webui = import ./../../containers/open-webui.nix;   #./containers/open-webui.nix;
  #   };
  # };
  # };

}

