{ lib, ... }: {
  # This file was populated at runtime with the networking
  # details gathered from the active system.
  networking = {
    nameservers = [ "67.207.67.2"
 "67.207.67.3"
 ];
    defaultGateway = "164.92.160.1";
    defaultGateway6 = {
      address = "2a03:b0c0:3:d0::1";
      interface = "eth0";
    };
    dhcpcd.enable = false;
    usePredictableInterfaceNames = lib.mkForce false;
    interfaces = {
      eth0 = {
        ipv4.addresses = [
          { address="164.92.164.113"; prefixLength=20; }
{ address="10.19.0.5"; prefixLength=16; }
        ];
        ipv6.addresses = [
          { address="2a03:b0c0:3:d0::7e:e001"; prefixLength=64; }
{ address="fe80::a00b:3aff:fe3a:4377"; prefixLength=64; }
        ];
        ipv4.routes = [ { address = "164.92.160.1"; prefixLength = 32; } ];
        ipv6.routes = [ { address = "2a03:b0c0:3:d0::1"; prefixLength = 128; } ];
      };
            eth1 = {
        ipv4.addresses = [
          { address="10.135.0.2"; prefixLength=16; }
        ];
        ipv6.addresses = [
          { address="fe80::a0bb:f9ff:fed6:418e"; prefixLength=64; }
        ];
        };
    };
  };
  services.udev.extraRules = ''
    ATTR{address}=="a2:0b:3a:3a:43:77", NAME="eth0"
    ATTR{address}=="a2:bb:f9:d6:41:8e", NAME="eth1"
  '';
}
