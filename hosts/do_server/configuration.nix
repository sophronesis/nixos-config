{inputs, config, pkgs, ... }: {
#@# hosts/do_server/configuration.nix: main configuration for Digital Ocean VPS
  imports = [
    ./hardware-configuration.nix
    ./networking.nix # generated at runtime by nixos-infect
    # inputs.home-manager.nixosModules.home-manager
    # ./../../modules
    ./../../modules/zsh.nix
  ];
  # nix.settings.experimental-features = [ "nix-command" "flakes" "ca-derivations" ];
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nixpkgs.config.allowUnfree = true;
  boot.tmp.cleanOnBoot = true;
  zramSwap.enable = true;
  networking.hostName = "doNixos";
  networking.domain = "";
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [ 
    ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIF9kIDv3xlBRkk8nD0i+1y/z6nYbmD/jw9LgKPCvYj1 sph@sphFW16Nixos''
    ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKbGicxeo6sPKcsker8gfhDYte8lhG4Hn9pH/ps4HEUX sph@sphHpNixos''
  ];
  environment.systemPackages = with pkgs; [
    neovim
    git
    # comma
    lsd # for ls
    nom # for nyx-upd
    # nix-index
    htop
    btop
    devenv
    direnv
    docker
    docker-compose
    tmux
    nmap
    tcpdump
    jq
    #python312
    ncdu
    wget
    (python312.withPackages (ps: with ps; [
      numpy
      scipy
      pip
    ]))
    
  ];
  services.tailscale.enable = true;
  virtualisation.docker = {
    enable = true;
		rootless.enable = true;
	};
  users.users.sph = {
    isNormalUser = true;
    initialPassword = "sph";
    extraGroups = [ "wheel" "docker" "networkmanager" ];
    openssh.authorizedKeys.keys = [
      ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIF9kIDv3xlBRkk8nD0i+1y/z6nYbmD/jw9LgKPCvYj1 sph@sphFW16Nixos''
      ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKbGicxeo6sPKcsker8gfhDYte8lhG4Hn9pH/ps4HEUX sph@sphHpNixos''
    ];
  };
  nix.settings.trusted-users = [ "sph" ];
  # Aggressive RAM optimization for DO droplet
  # OOM configuration:
  systemd = {
    # Create a separate slice for nix-daemon that is
    # memory-managed by the userspace systemd-oomd killer
    slices."nix-daemon".sliceConfig = {
      ManagedOOMMemoryPressure = "kill";
      ManagedOOMMemoryPressureLimit = "50%";
    };
    services."nix-daemon".serviceConfig.Slice = "nix-daemon.slice";

    # If a kernel-level OOM event does occur anyway,
    # strongly prefer killing nix-daemon child processes
    services."nix-daemon".serviceConfig.OOMScoreAdjust = 1000;
  };
  # temporarry
  # services.nginx = {
  #   enable = true;
  #   recommendedProxySettings = true;
  #   # recommendedTlsSettings = true;
  #   # other Nginx options
  #   virtualHosts."100.82.1.105:11434" =  {
  #     # enableACME = true;
  #     # forceSSL = true;
  #     locations."/" = {
  #       proxyPass = "http://127.0.0.1:11434";
  #       proxyWebsockets = true; # needed if you need to use WebSocket
  #       # extraConfig =
  #       #   # required when the target is also TLS server with multiple hosts
  #       #   # "proxy_ssl_server_name on;" +
  #       #   # required when the server wants to use HTTP Authentication
  #       #   "proxy_pass_header Authorization;"
  #       #   ;
  #     };
  #   };
  # };

  #@#   * list of modules to import with nix options
  nyx.zsh = {
    enable = true;
    stationID = "NIXDO";
    promptTheme = "Minimal";
    ntfyshChannel = "amogusamogus123321654456";
    s_ID = "D";
    flakePath = "~/.config/nixos"; # the only thing that should be symlinked imperatively
    flakeNixosID = "doNixos";
  };
  # programs.zsh.enable = true;
  # programs.zsh.history.size = 1000000;
  system.stateVersion = "23.11";
}
