if vim.loader and vim.fn.has "nvim-0.9.1" == 1 then vim.loader.enable() end
-- #@# config/nvim/init.lua: main neovim config (used astronvim as basis)
for _, source in ipairs {
  "astronvim.bootstrap",
  "astronvim.options",
  "astronvim.lazy",
  "astronvim.autocmds",
  "astronvim.mappings",
} do
  local status_ok, fault = pcall(require, source)
  if not status_ok then vim.api.nvim_err_writeln("Failed to load " .. source .. "\n\n" .. fault) end
end

if astronvim.default_colorscheme then
  if not pcall(vim.cmd.colorscheme, astronvim.default_colorscheme) then
    require("astronvim.utils").notify(
      ("Error setting up colorscheme: `%s`"):format(astronvim.default_colorscheme),
      vim.log.levels.ERROR
    )
  end
end

require("astronvim.utils").conditional_func(astronvim.user_opts("polish", nil, false), true)
require'nvim-treesitter.configs'.setup {
  markid = { enable = false }
}

-------------------
-- customization --
-------------------
-- vim.cmd('iab date@ <c-r>=strftime("[%Y/%m/%d-%a-%H:%M:%S]>")<cr>')
vim.cmd('iab date@ <c-r>=strftime("%Y/%m/%d-%a-%H:%M:%S")<cr>')
vim.cmd('ab ube@ #!/usr/bin/env')
-- sh -> zsh
vim.cmd("autocmd FileType sh map <buffer> <c-b> :w<CR>:exec '!zsh' shellescape(@%, 1)<CR>")
vim.cmd("autocmd FileType sh imap <buffer> <c-b> <esc>:w<CR>:exec '!zsh' shellescape(@%, 1)<CR>")
-- python building
vim.cmd("autocmd FileType python map <buffer> <c-b> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>")
vim.cmd("autocmd FileType python imap <buffer> <c-b> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>")
-- For ball_col
vim.cmd("autocmd FileType cpp map <buffer> <c-b> :w <cr>!cd build && cmake .. && make && ./main && cd ..<cr>")
vim.cmd("autocmd FileType cpp imap <buffer> <c-b> <esc>:w <cr>!cd build && cmake .. && make && ./main && cd ..<cr>")
-- For work1 go
vim.cmd("autocmd FileType go map <buffer> <c-b> :w<CR>:exec '!go run' shellescape(@%, 1)<CR>")
vim.cmd("autocmd FileType go imap <buffer> <c-b> <esc>:w<CR>:exec '!go run' shellescape(@%, 1)<CR>")
-- rust learning projects
vim.cmd("autocmd FileType rust map <buffer> <c-b> :w<CR>:exec '!cargo run'<CR>")
vim.cmd("autocmd FileType rust imap <buffer> <c-b> <esc>:w<CR>:exec '!cargo run'<CR>")
require'hop'.setup()
require'firenvim'
require('gitblame').setup {
     --Note how the `gitblame_` prefix is omitted in `setup`
    enabled = false,
}
require('log-highlight').setup()
-- require('log-highlight').setup {
--     -- The following options support either a string or a table of strings.
-- 
--     -- The file extensions.
--     extension = 'log',
-- 
--     -- The file names or the full file paths.
--     filename = {
--         'messages',
--     },
-- 
--     -- The file path glob patterns, e.g. `.*%.lg`, `/var/log/.*`.
--     -- Note: `%.` is to match a literal dot (`.`) in a pattern in Lua, but most
--     -- of the time `.` and `%.` here make no observable difference.
--     pattern = {
--         '/var/log/.*',
--         'messages%..*',
--     },
-- }
-- require"/home/sph/.local/share/nvim/lazy/nvim-ghost.nvim".setup()
-- require'/home/sph/.local/share/nvim/lazy/aw-watcher-vim/plugin/activitywatch.vim'.setup()
-- vim.cmd(':Lazy load aw-watcher-vim')
-- vim.cmd('AWStart')
-- require("lazy").load({"ActivityWatch/aw-watcher-vim"})

vim.keymap.set('n', '<a-q>', ':HopWord<CR>')
vim.keymap.set('n', '<a-w>', ':HopChar2<CR>')
