return {
  'f-person/git-blame.nvim',
  'David-Kunz/markid',
  {
    'fei6409/log-highlight.nvim',
    config = function()
      require('log-highlight').setup {}
    end
  },
  -- {
  --   'subnut/nvim-ghost.nvim',
  --   lazy=false
  -- }
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  -- "andweeb/presence.nvim",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
}
