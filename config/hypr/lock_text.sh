#!/usr/bin/env sh

if [[ "$1" == "LT" ]]; then
    # just mpris data
    MPRIS=$(~/.config/waybar/sb_mpris.sh 2>/dev/null | head -n 1)
    NOTIF_C=$(swaync-client -c)
    NOTIF_ICO="󱅫"
    if [[ "$NOTIF_C" == 0 ]]; then
        NOTIF_ICO=""
    fi  
    NOTIF="$NOTIF_ICO $NOTIF_C"
    echo "  $NOTIF  $MPRIS"
fi

if [[ "$1" == "RT" ]]; then
    # battery + weather + kbd
    BAT=$(~/.config/waybar/sb_battery.sh 2>/dev/null | head -n 1)
    KBD="  $(~/.config/waybar/sb_kbdlayout.sh 2>/dev/null | head -n 1)"
    WTR="$(~/.config/waybar/sb_weather.sh | jq -r ".text")"
    echo "$WTR  $KBD  $BAT"
fi

if [[ "$1" == "LB" ]]; then
    echo "$(~/.config/waybar/sb_hwstat.sh | jq -r ".tooltip")"
fi

if [[ "$1" == "RB" ]]; then
    echo "$(~/.config/waybar/sb_weather.sh | jq -r ".tooltip")" | sed -E "s/<b>//" | sed -E "s/<\/b>//"
fi
