#!/usr/bin/env sh
#BATID=0
BATID=$(acpi | grep -v " 0%" | awk '{print $2}' | cut -c 1)
# percentage=$(acpi | grep $BATID: | cut -d " " -f 4-4 | cut -d "%" -f 1-1)
percentage=$(acpi | grep " $BATID" | cut -d"," -f 2- | cut -d" " -f 2- | cut -d"%" -f 1)
state=$(acpi | grep $BATID: | cut -d " " -f 3-3 | cut -d "," -f 1-1)
bonusinfo=$(acpi -V | grep "0: desi" | cut -c 12-)

batlogpath="/home/sph/.local/share/bat.log"
timestamp=$(date +%s)
cpu_load=$(echo ""$[100-$(vmstat 1 2|tail -1|awk '{print $15}')]"")
gpu_load=$(nvidia-smi | grep Default | awk '{print $13}' | cut -d"%" -f 1)
brightness=$(cat /sys/class/backlight/intel_backlight/brightness)
totalCharge=$(cat /sys/class/power_supply/BAT0/charge_full)
curCharge=$(cat /sys/class/power_supply/BAT0/charge_now)
vNow=$(cat /sys/class/power_supply/BAT0/voltage_now)
vMin=$(cat /sys/class/power_supply/BAT0/voltage_min_design)
echo "$timestamp $cpu_load $gpu_load $brightness $percentage $state $totalCharge $curCharge $vNow $vMin" >> $batlogpath

symb=""
[ "$percentage" -le 80 ] && symb=""
[ "$percentage" -le 60 ] && symb=""
[ "$percentage" -le 40 ] && symb=""
[ "$percentage" -le 20 ] && symb=""
[ "$percentage" -le  0 ] && symb=""
[ "$state" == "Charging" ] && isCharge="󱐋"
[ "$state" == "Full" ] && isCharge="󱐋"
echo "$symb  $percentage%$isCharge"
echo "$bonusinfo"
