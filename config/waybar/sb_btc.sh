#!/usr/bin/env sh

currency="usd" # usd/gbp/eur/btc available
interval="@30d"	# History contained in chart preceded by '@' (7d = 7 days)
url="btc" # name of currency in url
dir="${XDG_DATA_HOME:-$HOME/.local/share}/crypto-prices"
pricefile="$dir/$url-$currency"
chartfile="$dir/$url-$currency-chart"

test_connection() {
  RES=$(curl -sm1 "btc.rate.sx/1usd")
  PATTERN="<html>"
  if [ -z "$RES" ] ; then
    echo "0"
    exit 0
  fi
  if grep -q "$PATTERN" <<< "$RES" ; then
    echo "0"
  else
    echo "1"
  fi
}

ISONLINE=$(test_connection)

updateprice() { 
  if [[ "$ISONLINE" -eq "1" ]]; then
    temp="$(mktemp)"
	  curl -sm 1 "$currency.rate.sx/1$url" > "$temp" &&
		mv -f "$temp" "$pricefile" &&
		curl -sm 1 "$currency.rate.sx/$url$interval" > "$temp" &&
		mv -f "$temp" "$chartfile"
  fi
}

showprice() {
  PRICE=$(printf "%0.2f" "$(cat "$pricefile")")
  if [[ "$ISONLINE" -eq "0" ]]; then
    #STATUS="%{F#E57C46}󰓦%{F-}"
    # STATUS=" %{F#E57C46}%{F-}"
    STATUS=" "
  else
    STATUS=""
  fi
  echo "$PRICE$STATUS"
}

[ -d "$dir" ] || mkdir -p "$dir"

updateprice "$url"
showprice
