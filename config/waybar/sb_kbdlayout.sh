#language=$(swaymsg -r -t get_inputs | awk '/1:1:AT_Translated_Set_2_keyboard/;/xkb_active_layout_name/' | grep -A1 '\b1:1:AT_Translated_Set_2_keyboard\b'    | grep "xkb_active_layout_name" | awk -F '"' '{print $4}')
#echo $language
# hyprctl devices -j | jq -r ".keyboards[] | select( .main == true) |.active_keymap" | cut -c1-2 | tr "a-z" "A-Z"

# hp pavilion
fullKBName=$(hyprctl devices -j | jq -r '.keyboards[] | select(.name == "at-translated-set-2-keyboard") |.active_keymap') 

# framework 16
[ -z "$fullKBName" ] && \
  fullKBName=$(hyprctl devices -j | jq -r '.keyboards[] | select(.name == "framework-laptop-16-keyboard-module---ansi-keyboard") |.active_keymap') && \

shortKBName=$(echo $fullKBName | cut -c1-2 | tr "a-z" "A-Z")

echo $shortKBName
echo $fullKBName
