#!/usr/bin/env bash

pinglogpath="/home/sph/.local/share/ping.log"
infSymb="∞"
network=$(ip route get 1.1.1.1 | grep -Po '(?<=dev\s)\w+' | cut -f1 -d ' ')
isecs=1
iface=$network # wlo1
## speed estimation is based on https://forum.garudalinux.org/t/netspeed-in-waybar/26385/4
# `human_readable` adapted from https://gist.github.com/cjsewell/96463db7fec6faeab291
human_readable() {
  local value=$1
  local units=( bit Kbit Mbit Gbit Tbit Pbit )
  local index=0
  while (( value > 1024 && index < 5 )); do
        (( value /= 1024, index++ ))
  done
  echo "$value${units[$index]}"
}

snore() {
    local IFS
    [[ -n "${_snore_fd:-}" ]] || { exec {_snore_fd}<> <(:); } 2>/dev/null
    read ${1:+-t "$1"} -u $_snore_fd || :
}

# first measure to start with
declare -a traffic_prev traffic_curr traffic_delta
traffic_prev=( 0 0 0 0  0 0 0 0 )
traffic_curr=( $(awk '/^ *'${iface}':/{print $2 " " $3 " " $4 " " $5 " " $10 " " $11 " " $12 " " $13}' /proc/net/dev) )
for i in {0..7}; do
  (( traffic_delta[i] = 10 * ( traffic_curr[i] - traffic_prev[i] ) / isecs ))
done
traffic_prev=(${traffic_curr[@]})
snore 1
pingRes=$(timeout 1 zsh -c "ping -c 1 8.8.8.8 | head -2 | tail -1 | cut -d' ' -f 7 | cut -c 6-" || echo $infSymb) # echo ">1k")
interface_easyname=$(dmesg | grep $network | grep renamed | awk 'NF>1{print $NF}')
activssid=$(nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\' -f2 | cut -c5-)
strnNum=$(nmcli -t -f active,SIGNAL dev wifi | grep yes | cut -c 5-)

# second measure to calculate delta
traffic_curr=( $(awk '/^ *'${iface}':/{print $2 " " $3 " " $4 " " $5 " " $10 " " $11 " " $12 " " $13}' /proc/net/dev) )
for i in {0..7}; do
  (( traffic_delta[i] = 10 * ( traffic_curr[i] - traffic_prev[i] ) / isecs ))
done
traffic_prev=(${traffic_curr[@]})

ISTUN="0"
if ! [ $network ]
then
   network_active="⛔"
elif [[ $network == *"wlan"* || $network == *"wlo"* || $network == *"wlp"* ]]
then
   interface_easyname=$network
   strn="󰤭 "
   [ "$strnNum" -le 100 ] && strn="󰤨 "
   [ "$strnNum" -le  80 ] && strn="󰤥 "
   [ "$strnNum" -le  60 ] && strn="󰤢 "
   [ "$strnNum" -le  40 ] && strn="󰤟 "
   [ "$strnNum" -le  20 ] && strn="󰤯 "
   network_active=$strn
elif [[ $network == *"tun"* ]]
then
   ISTUN="1"
   interface_easyname=$network
   network_active="󰌘 "
else
   network_active=""
fi

N_LAUNCHED=$(ps -aux | grep popcorntime | grep /gen | wc -l)
[ "$N_LAUNCHED" -ge 1 ] && [ "$ISTUN" -le 0 ] && \
   notify-send -u critical "WARNING:" "Using popcorntime without VPN tunnel" &&
   kill $(ps -aux | grep popcorntime | grep /gen | cut -f8 -d$' ')


echo "$pingRes" >> $pinglogpath 
missdNum=$(cat /home/sph/.local/share/ping.log | tail -n 8 | grep $infSymb | wc -l)

# braile ico:
PINGTAIL_F=$(mktemp)
PINGMASK_F=$(mktemp)
# get last 8 pings
cat $pinglogpath | tail -n 8 > $PINGTAIL_F
# convert to mask
while IFS= read -r line; do
    if [[ $line =~ $infSymb ]]; then
        echo 1 >> $PINGMASK_F
    else
        echo 0 >> $PINGMASK_F
    fi
done < $PINGTAIL_F 
# combine mask with powers on 2
OFFSET=$(paste -d '*' ~/.config/waybar/powers $PINGMASK_F | bc | paste -sd+ | bc)
OFFSET=$((OFFSET + 1))
missdIco=$(cat ~/.config/waybar/braileBin | sed -n $(echo $OFFSET)p)
[ "$missdNum" -le 8 ] && missdClr="red"
[ "$missdNum" -le 7 ] && missdClr="red"
[ "$missdNum" -le 6 ] && missdClr="orange"
[ "$missdNum" -le 5 ] && missdClr="orange"
[ "$missdNum" -le 4 ] && missdClr="orange"
[ "$missdNum" -le 3 ] && missdClr="yellow"
[ "$missdNum" -le 2 ] && missdClr="yellow"
[ "$missdNum" -le 1 ] && missdClr="yellow"
[ "$missdNum" -le 0 ] && missdClr="green"

# round ico:

# [ "$missdNum" -le 8 ] && missdIco="󰄰" && missdClr="red"
# [ "$missdNum" -le 7 ] && missdIco="󰪞" && missdClr="red"
# [ "$missdNum" -le 6 ] && missdIco="󰪟" && missdClr="orange"
# [ "$missdNum" -le 5 ] && missdIco="󰪠" && missdClr="orange"
# [ "$missdNum" -le 4 ] && missdIco="󰪡" && missdClr="orange"
# [ "$missdNum" -le 3 ] && missdIco="󰪢" && missdClr="yellow"
# [ "$missdNum" -le 2 ] && missdIco="󰪣" && missdClr="yellow"
# [ "$missdNum" -le 1 ] && missdIco="󰪤" && missdClr="yellow"
# [ "$missdNum" -le 0 ] && missdIco="󰪥" && missdClr="green"


# TEXT="$network_active $interface_easyname <span foreground=\"red\">َ$missdIco</span>  ($pingRes ms)"

DOWNS=$(human_readable ${traffic_delta[0]})
UPS=$(human_readable ${traffic_delta[4]})
SPEED=$(printf '%5s⇣ %5s⇡' $DOWNS $UPS)

#TEXT="<span foreground=\\\"$missdClr\\\">$missdIco</span> $network_active $interface_easyname"
TEXT="<span foreground=\\\"$missdClr\\\">$missdIco</span> $network_active"
TOOLTIP="$missdIco  - $missdNum/8 missed packets\nping: $pingRes ms\nssid: $activssid (signal: $strnNum)\nspeed: $SPEED\nspeed (raw): ${traffic_delta[0]}/${traffic_delta[4]}\ninterface: $interface_easyname"

echo "{}" | jq -c ".text = \"$TEXT\"| .tooltip = \"$TOOLTIP\""

#echo "$network_active $interface_easyname ($pingRes ms)"
#echo -e "ssid: $activssid (signal: $strnNum)"
# echo "$network $network_active $interface_easyname $ping\nactive ssid = "
