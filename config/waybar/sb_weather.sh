#!/usr/bin/env sh

weatherreport_short="${XDG_DATA_HOME:-$HOME/.local/share}/.wttr.in.cmd"
#LOCATION="Ukraine,%20Kyiv" # leave empty to autodetect
LOCATION="Munich" # leave empty to autodetect

test_connection() {
  RES=$(curl -sm1 "wttr.in/")
  PATTERN="<html>"
  if [ -z "$RES" ] ; then
    echo "0"
    exit 0
  fi
  if grep -q "$PATTERN" <<< "$RES" ; then
    echo "0"
  else
    echo "1"
  fi
}

ISONLINE=$(test_connection)

updateweather() {     
  if [[ "$ISONLINE" -eq "1" ]]; then
    temp="$(mktemp)"
    #curl -sm1 "wttr.in/$LOCATION" > "$temp" &&
	  #mv -f "$temp" "$weatherreport" &&
	  curl -sm1 "wttr.in/$LOCATION?T0m&format=%l:%t:%w:%m:%M" > "$temp" &&
	  mv -f "$temp" "$weatherreport_short";
  fi
}

showweather() {
  reportData=$(cat $weatherreport_short) &&
  LOCC=$(echo $reportData | cut -d ":" -f1 ) &&
  TMPP=$(echo $reportData | cut -d ":" -f2 ) &&
  WIND=$(echo $reportData | cut -d ":" -f3 ) &&
  MOON=$(echo $reportData | cut -d ":" -f4 ) &&
  MDAY=$(echo $reportData | cut -d ":" -f5 )
  if [[ "$ISONLINE" -eq "0" ]]; then
    STATUS=" "
    #STATUS=" %{F#E57C46}?%{F-}"
  else
    STATUS=""
  fi
  # echo "$LOCC: $TMPP $WIND $MOON $STATUS"
  MDAY="$MDAY""d"
  echo "🌡️$TMPP 💨 $WIND $MOON $MDAY $STATUS"
}

getTooltip() {
  if [ -n $1 ]; then
    timeout 3 wttrbar 2>/dev/null || echo "{}"
  else
    timeout 3 wttrbar --location $1 2>/dev/null || echo "{}"
  fi
}

updateweather
text="$(showweather)"
echo $(getTooltip $LOCATION) | jq -c  ".text = \"$text\""
# update bd with weather
# make_bg_with_weather $SOURCE_BG 
