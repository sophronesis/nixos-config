#!/usr/bin/env sh

NIXINFO=$(nix-info | sed 's/\"//g' | sed 's/, /@/' | sed 's/, /@/' | sed 's/, /@/' | sed 's/@/\\n  /g' | sed 's/, nixp/\\n  nixp/')
INSTALLDATE=$(stat / | grep "Birth" | sed 's/ Birth: //g' | cut -c -19)
# UPTIME=$(uptime | awk -F'( |,|:)+' '{d=h=m=0; if ($7=="min") m=$6; else {if ($7~/^day/) {d=$6;h=$8;m=$9} else {h=$6;m=$7}}} {print d+0,"days,",h+0,"hours,",m+0,"minutes."}')
MAXPROFILE=$(ls /nix/var/nix/profiles | grep system | cut -c 8- | rev | cut -c 6- | rev | sort -n | tail -1)
# DFSIZE=$(df -h | grep "^/dev/" | cut -c 6- | sed -e 's/\t/  /' | sed -e 's/^/  /')
NEOFETCH=$(neofetch | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g" | cut -c 6- | tail -20 | head -13  | awk '{printf "%s\\n  ", $0}' | sed 's/: waybar/: kitty/' | sed -e 's/^/  /')
# SENSORS=$(sensors | awk '{printf "%s\\n  ", $0}')

# TOOLTIP="* current generation: $MAXPROFILE\\n* install date: $INSTALLDATE\\n* nix-info:\\n  $NIXINFO\\n* neofetch:\\n$NEOFETCH\\n* lm_sensors:\\n  $SENSORS"
TOOLTIP="* current generation: $MAXPROFILE\\n* install date: $INSTALLDATE\\n* nix-info:\\n  $NIXINFO\\n* neofetch:\\n$NEOFETCH"



# NIXINFO=$(printf "%q" $NIXINFO)
#| sed -e 's/, /\\n/g')

echo "{\"text\":\" \", \"tooltip\":\"$TOOLTIP\"}" | jq -c # $NIXINFO\"}"

