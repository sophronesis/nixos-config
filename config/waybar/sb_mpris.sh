#!/usr/bin/env sh
playerName=$(playerctl metadata -f "{{playerName}}" 2>/dev/null)
media=$(playerctl metadata -f "{{artist}} - {{title}}" 2>/dev/null)
player_status=$(playerctl status 2>/dev/null)

# Check players are available and playing
plAvail="N"
song_status='No media playing'
[ "$player_status" = "Playing" ] && song_status='' && plAvail="Y"
[ "$player_status" = "Paused" ]  && song_status='' && plAvail="Y"

# Replace known players with nerdfont icons
playerStr=" ($playerName)"
[ "$playerName" = "TelegramDesktop" ] && playerStr=' '
[ "$playerName" = "firefox" ]         && playerStr=' '
[ "$playerName" = "chromium" ]        && playerStr=' '
[ "$playerName" = "ncspot" ]          && playerStr=' '

# Prints percentage to be completed as symbol
SONGLENGTH=$(playerctl metadata --format "{{mpris:length}}")
SONGPOS=$(playerctl position)
SONGPERCENT=$(echo $SONGLENGTH | awk "{print int(100 * 1000000 * $SONGPOS / (\$1))}" 2>/dev/null)
[ "a$SONGPERCENT" = "a" ] && SONGPERCENT="-1"
# not as accurate, but able to capture all 9 positions
[ "$SONGPERCENT" -le 100 ] && song_part="󰪥 "
[ "$SONGPERCENT" -le 88  ] && song_part="󰪤 "
[ "$SONGPERCENT" -le 77  ] && song_part="󰪣 "
[ "$SONGPERCENT" -le 66  ] && song_part="󰪢 "
[ "$SONGPERCENT" -le 55  ] && song_part="󰪡 "
[ "$SONGPERCENT" -le 44  ] && song_part="󰪠 "
[ "$SONGPERCENT" -le 33  ] && song_part="󰪟 "
[ "$SONGPERCENT" -le 22  ] && song_part="󰪞 "
[ "$SONGPERCENT" -le 11  ] && song_part="󰄰 "
[ "$SONGPERCENT" -le -1   ] && song_part="" #󱃓 "
#[ "$SONGPERCENT" -le 100 ] && song_part="󰪥 "
#[ "$SONGPERCENT" -le 88  ] && song_part="󰪤 "
#[ "$SONGPERCENT" -le 75  ] && song_part="󰪣 "
#[ "$SONGPERCENT" -le 63  ] && song_part="󰪢 "
#[ "$SONGPERCENT" -le 50  ] && song_part="󰪡 "
#[ "$SONGPERCENT" -le 38  ] && song_part="󰪠 "
#[ "$SONGPERCENT" -le 25  ] && song_part="󰪟 "
#[ "$SONGPERCENT" -le 13  ] && song_part="󰪞 "
#[ "$SONGPERCENT" -le 0   ] && song_part="" #󱃓 "

# Print result only if players are available
[ $plAvail = "Y" ] && outStr="♫ $song_status $song_part$playerStr $media"

echo "$outStr"
echo "$SONGPERCENT% of song"
