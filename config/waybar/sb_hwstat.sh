#!/usr/bin/env bash

perc2Ico() {
  [ "$1" -ge 0  ] && ico="󰄰"
  [ "$1" -ge 11 ] && ico="󰪞"
  [ "$1" -ge 22 ] && ico="󰪟"
  [ "$1" -ge 33 ] && ico="󰪠"
  [ "$1" -ge 44 ] && ico="󰪡"
  [ "$1" -ge 55 ] && ico="󰪢"
  [ "$1" -ge 66 ] && ico="󰪣"
  [ "$1" -ge 77 ] && ico="󰪤"
  [ "$1" -ge 88 ] && ico="󰪥"
  echo $ico
}

perc2Clr() {
  [ "$1" -ge 0  ] && clr="green"
  [ "$1" -ge 11 ] && clr="green"
  [ "$1" -ge 22 ] && clr="green"
  [ "$1" -ge 33 ] && clr="yellow"
  [ "$1" -ge 44 ] && clr="yellow"
  [ "$1" -ge 55 ] && clr="yellow"
  [ "$1" -ge 66 ] && clr="orange"
  [ "$1" -ge 77 ] && clr="orange"
  [ "$1" -ge 88 ] && clr="red"
  echo $clr
}

createRBar() {
  val=$1
  clr=$2
  ico=$(perc2Ico $val)
  [ -z $clr ] && clr=$(perc2Clr $val)
  echo "<span foreground=\\\"$clr\\\">$ico</span>"
}

createTT() {
  val=$1
  pbar=$2
  desc=$3
  echo "$pbar  - $val% $desc"
}

CPU_LOAD=""$[100-$(vmstat 1 2|tail -1|awk '{print $15}')]""
# for nvidia gpus:
command -v nvidia-smi >/dev/null 2>&1 && \
  GPU_LOAD=$(nvidia-smi | grep Default | awk '{print $13}' | cut -d"%" -f 1)
# for amd gpu:
command -v rocm-smi >/dev/null 2>&1 && \
  GPU_LOAD=$(rocm-smi 2>/dev/null | grep ^0 | awk '{print $10}' | cut -d"%" -f 1)
MEM_LOAD=$(printf "%.0f\n" $(free -m | grep Mem | awk '{print ($3/$2)*100}'))
SSD_LOAD=$(df -h | grep " /$" | awk '{print ($5)}' | tr -d '%')
SND_LOAD=$(amixer sget Master | grep 'Front Left:' | awk -F'[][]' '{ print $2 }' | sed 's/%//')
[ -z "$SND_LOAD" ] && \
  SND_LOAD=$(amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $2 }' | sed 's/%//')
# for hp pavilion
[ -d "/sys/class/backlight/intel_backlight/" ] && \
    BKL_LOAD=$(awk "BEGIN {print int($(cat /sys/class/backlight/intel_backlight/brightness)/7500*100)}")
# for framework 16
[ -d "/sys/class/backlight/amdgpu_bl2/" ] && \
  BKL_LOAD=$(awk "BEGIN {print int($(cat /sys/class/backlight/amdgpu_bl2/brightness)/255*100)}")

CPU_PBAR=$(createRBar "$CPU_LOAD")
GPU_PBAR=$(createRBar "$GPU_LOAD")
MEM_PBAR=$(createRBar "$MEM_LOAD")
SSD_PBAR=$(createRBar "$SSD_LOAD")
SND_PBAR=$(createRBar "$SND_LOAD" "#20a7db")
BKL_PBAR=$(createRBar "$BKL_LOAD" "white")
TEXT="$CPU_PBAR $GPU_PBAR $MEM_PBAR $SSD_PBAR $SND_PBAR $BKL_PBAR"

CPU_TT=$(createTT $CPU_LOAD $(perc2Ico $CPU_LOAD) "CPU current load")
GPU_TT=$(createTT $GPU_LOAD $(perc2Ico $GPU_LOAD) "GPU current load")
MEM_TT=$(createTT $MEM_LOAD $(perc2Ico $MEM_LOAD) "RAM usage")
SSD_TT=$(createTT $SSD_LOAD $(perc2Ico $SSD_LOAD) "Disk usage at \\\"/\\\" folder")
SND_TT=$(createTT $SND_LOAD $(perc2Ico $SND_LOAD) "Volume")
BKL_TT=$(createTT $BKL_LOAD $(perc2Ico $BKL_LOAD) "Backlight of monitor")
TOOLTIP="$CPU_TT\n$GPU_TT\n$MEM_TT\n$SSD_TT\n$SND_TT\n$BKL_TT"

echo "{}" | jq -c ".text = \"$TEXT\"| .tooltip = \"$TOOLTIP\""
