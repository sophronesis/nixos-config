#!/usr/bin/env sh
CONTCOUNT=$(docker ps -q | wc -l)
type docker >/dev/null || CONTCOUNT="N/A" # if docker is not available
echo -e "  $CONTCOUNT\nDocker containers running"
