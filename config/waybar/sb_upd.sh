#!/usr/bin/env bash

cd ~/.config/nixos/

# DERIVS=$(nixos-rebuild dry-build --flake .#sphFW16Nixos 2>&1 | grep "  /nix/store/")
# DERIVS_N=$(printf "$DERIVS" | wc -l)
# DERIVS_NAMES=$(echo "$DERIVS" | cut -c 47- | tr '\n' '|' | sed 's/|/\\n  /g')
DERIVS_N="N/A" # =$(nixos-rebuild dry-build --flake .#sphFW16Nixos 2>&1 | grep "  /nix/store/" | wc -l)
DERIVS_NAMES="N/A"  # $(nixos-rebuild dry-build --flake .#sphFW16Nixos 2>&1 | grep "  /nix/store/" | cut -c 47- | tr '\n' '|' | sed 's/|/\\n  /g')

# Fetch updates from the remote repository
git fetch 2>/dev/null

# Check if there are any new commits on the remote branch
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse @{u})
BASE=$(git merge-base @ @{u})

if [ $LOCAL = $REMOTE ]; then
    REPOTEXT="Your branch is up to date with the remote branch."
    REPOICON=""
    REPOCLR="green"
elif [ $LOCAL = $BASE ]; then
    REPOTEXT="Updates are available. You can pull the latest changes."
    REPOICON=""
    REPOCLR="yellow"
elif [ $REMOTE = $BASE ]; then
    REPOTEXT="You have local changes that need to be pushed."
    REPOICON=""
    REPOCLR="yellow"
else
    REPOTEXT="Your branch and the remote branch have diverged."
    REPOICON=""
    REPOCLR="orange"
fi
# echo "$REPOICON"
# echo "$REPOTEXT"

TEXT="<span foreground=\\\"$REPOCLR\\\">$REPOICON </span>   $DERIVS_N"
TOOLTIP="$REPOICON: $REPOTEXT\n  $DERIVS_N derivations to build:\n  $DERIVS_NAMES"

echo "{}" | jq -c ".text = \"$TEXT\"| .tooltip = \"$TOOLTIP\""
