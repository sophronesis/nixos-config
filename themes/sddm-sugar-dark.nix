{ pkgs }:
  #@# themes/sddm-sugar-dark.nix: sugar dark theme for sddm display manager 
let
  # imgLink = "https://YOURIMAGELINK/image.png";
  imgLink = "https://github.com/DaringCuteSeal/wallpapers/raw/gh-pages/nature/beach/beach.png";
  image = pkgs.fetchurl {
    url = imgLink;
    sha256 = "1waai348rl7rxdvhj1bz6328pafi8mxbk0q26dqgy4lszaadr15l";
  };
in
pkgs.stdenv.mkDerivation {
  name = "sddm-sugar-dark";
  src = pkgs.fetchFromGitHub {
    owner = "MarianArlt";
    repo = "sddm-sugar-dark";
    rev = "ceb2c455663429be03ba62d9f898c571650ef7fe";
    sha256 = "0153z1kylbhc9d12nxy9vpn0spxgrhgy36wy37pk6ysq7akaqlvy";
  };
  installPhase = ''
    mkdir -p $out
    cp -R ./* $out/
    cd $out/
    rm Background.jpg
    cp -r ${image} $out/Background.jpg
   '';
}

