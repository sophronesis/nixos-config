{ pkgs }:
#@# themes/sddm-aerial.nix: aerial theme for sddm display manager 
let
  # imgLink = "https://YOURIMAGELINK/image.png";
  # imgLink = "https://github.com/DaringCuteSeal/wallpapers/raw/gh-pages/nature/beach/beach.png";
  # https://raw.githubusercontent.com/D3Ext/aesthetic-wallpapers/main/images/nix.png
  # imgLink = "https://raw.githubusercontent.com/D3Ext/aesthetic-wallpapers/main/images/cute-town-dark.png";
  # https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/the-gate-to-serenity-danisogen.png

  # https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/acoolrocket-dalle2-hokusai-non-prompt-landscape.png
  # https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/rhads-survivors.png
  # https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/junhyuk-lim-acoolrocket-tree-of-life-edit.png
  # https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/refiend-carmine-rock.jpg
  #imgLink = "https://raw.githubusercontent.com/DenverCoder1/minimalistic-wallpaper-collection/main/images/rhads-survivors.png";
  # imgLink = "https://raw.githubusercontent.com/D3Ext/aesthetic-wallpapers/main/images/abstract.jpg";
  # imgLink = "https://raw.githubusercontent.com/D3Ext/aesthetic-wallpapers/main/images/oled-mountains.jpg";
  imgLink = "https://raw.githubusercontent.com/D3Ext/aesthetic-wallpapers/main/images/trees.png";
  image = pkgs.fetchurl {
    url = imgLink;
    # sha256 = "0lq3sh6dab6g5waa9vnc8xmzwqy2ca0hka8ghnck15cyxbmd765b";
    sha256 = "sha256-Cda+PjMmAm8yv6yw+VBo5FwAQnehppInwgkHGpDHM4s=";
  };
in
pkgs.stdenv.mkDerivation {
  name = "sddm-aerial";
  src = pkgs.fetchFromGitHub {
    owner = "3ximus";
    repo = "aerial-sddm-theme";
    rev = "92b85ec7d177683f39a2beae40cde3ce9c2b74b0";
    sha256 = "sha256-0MdjhuftOrIDnqMvADPU9/55s/Upua9YpfIz0wpGg4E=";
  };
  installPhase = ''
    mkdir -p $out
    cp -R ./* $out/
    cd $out/
    rm background.jpg
    cp -r ${image} $out/background.jpg
  '';
}

