# Nixos Config

![preview](./images/preview_nix.png)


Here is my config for Framework 16 and HP Pavilion 

|   tool  |     package     |
|---------|-----------------|
|os       |nixos            |
|wm       |hyprland         | 
|shell    |zsh              |
|terminal |kitty            |  
|editor   |nvim (astronvim) |
|statusbar|waybar           | 
|locker   |swaylock, wlogout| 

To read fresh colorful documentation of features for each module please run:

```$ ./scripts/documentation.sh```

Colorless static version available [here](docs.txt)

