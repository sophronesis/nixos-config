#!/usr/bin/env sh
#@# scripts/maintain: list of usefull small scripts to service nixos system

# MODE="${MODE:-help}"
MODE="${1:-help}"
DIRNAME="/home/sph/.config/nixos"
REBUILD_CMD="nixos-rebuild switch --cores 16 --max-jobs 8 --flake .#sphFW16Nixos"
REBUILD_CMD_MVPN="nixos-rebuild switch --cores 16 --max-jobs 8 --flake .#sphFW16Nixos --specialisation mvpn"
REBUILD_CMD_MC="nixos-rebuild switch --cores 16 --max-jobs 8 --flake .#sphFW16Nixos --specialisation mc"
REBUILD_CMD_BOOT="nixos-rebuild --install-bootloader boot --cores 16 --max-jobs 8 --flake .#sphFW16Nixos"
REBUILD_CMD_DO="nixos-rebuild --flake .#doNixos --cores 16 --target-host d --use-remote-sudo switch"

pushd $DIRNAME > /dev/null

[ "$MODE" == "help" ] && \
  echo "maintanance script for nixos setup" && \
  echo " docs            - view documentation for modules in this nixos configuration" && \
  echo " nix_switch      - switch generation of nixos" && \
  echo " nix_switch_raw  - switch generation of nixos without nix output monitor" && \
  echo " nix_switch_mvpn - switch generation of nixos with mullvad vpn specialisation" && \
  echo " nix_switch_mc   - switch generation of nixos with minecraft server" && \
  echo " nix_switch_boot - switch generation of nixos and update bootloader" && \
  echo " nix_switch_do   - switch generation of nixos on remote DO server" && \
  echo " nix_storegc     - garbage collect on nix store" && \
  echo " nix_storegchard - garbage collect on nix store for all but current generation" && \
  echo " nix_storeopt    - optimize nix store" && \
  echo " nix_listgen     - list generations" &&\
  echo " lock_backup     - backup current flake.lock file" && \
  echo " lock_restore    - restore last flake.lock file" && \
  echo " lock_update     - update lock file" && \
  echo " lock_switch     - lock_backup + lock_update + nix_switch" && \
  echo " index           - update nix index (used for command not found func)" && \
  echo " shell           - enter nix shell with packages for install" # && \

[ "$MODE" == "docs" ] && bash ./scripts/documentation.sh

[ "$MODE" == "lock_backup" ] && \
  LOCKNAME="$(date +"%s").lock" && \
  cp --preserve=timestamps flake.lock "backup_locks/$LOCKNAME" && \
  echo "flack.lock has been backed up as backup_locks/$LOCKNAME"

[ "$MODE" == "lock_restore" ] && \
  LOCKNAME=$(find backup_locks | sort | tail -1) && \
  rm flake.lock && \
  cp --preserve=timestamps "$LOCKNAME" flake.lock && \
  echo "Backup flake.lock file ($LOCKNAME) had been restored"

[ "$MODE" == "lock_update" ] && nix flake update
[ "$MODE" == "lock_switch" ] && \
  echo "########################" && \
  echo "1. Backing up nix flake:" && \
  echo "########################" && \
  LOCKNAME="$(date +"%s").lock" && \
  cp --preserve=timestamps flake.lock "backup_locks/$LOCKNAME" && \
  echo "flack.lock has been backed up as backup_locks/$LOCKNAME" && \
  echo "##############################" && \
  echo "2. Updating current nix flake:" && \
  echo "##############################" && \
  nix flake update && \
  echo "###############################" && \
  echo "3. Switching to new generation:" && \
  echo "###############################" && \
  sudo echo -e "\c" && \
  $(sudo $REBUILD_CMD |& nom)
[ "$MODE" == "nix_switch" ] && \
  sudo echo -e "\c" && \
  $(sudo $REBUILD_CMD |& nom)

[ "$MODE" == "nix_switch_raw" ] && \
  $(sudo $REBUILD_CMD)

[ "$MODE" == "nix_switch_mvpn" ] && \
  $(sudo $REBUILD_CMD_MVPN)

[ "$MODE" == "nix_switch_mc" ] && \
  $(sudo $REBUILD_CMD_MC)

[ "$MODE" == "nix_switch_boot" ] && \
  $(sudo $REBUILD_CMD_BOOT)

[ "$MODE" == "nix_switch_do" ] && \
  $($REBUILD_CMD_DO)

[ "$MODE" == "nix_storegc" ] && nix-collect-garbage --delete-older-than 7d
[ "$MODE" == "nix_storegchard" ] && nix-collect-garbage -d
[ "$MODE" == "nix_storeopt" ] && nix store optimise
[ "$MODE" == "nix_listgen" ] && nixos-rebuild list-generations
[ "$MODE" == "index" ] && nix-index
[ "$MODE" == "shell" ] && nix-shell -p tmux neovim git

popd > /dev/null
