#!/usr/bin/env sh
#@# scripts/documentation.sh: i think we went meta here
pushd "$(dirname "$0")""/../"
# Function to color text matching a regular expression
color_text() {
  local text="$1"
  local regex="$2"
  local color="$3"

  # Define color codes
  case "$color" in
    black) color_code='\033[0;30m' ;;
    red) color_code='\033[0;31m' ;;
    green) color_code='\033[0;32m' ;;
    yellow) color_code='\033[0;33m' ;;
    blue) color_code='\033[0;34m' ;;
    purple) color_code='\033[0;35m' ;;
    cyan) color_code='\033[0;36m' ;;
    white) color_code='\033[0;37m' ;;
    bold_black) color_code='\033[1;30m' ;;
    bold_red) color_code='\033[1;31m' ;;
    bold_green) color_code='\033[1;32m' ;;
    bold_yellow) color_code='\033[1;33m' ;;
    bold_blue) color_code='\033[1;34m' ;;
    bold_purple) color_code='\033[1;35m' ;;
    bold_cyan) color_code='\033[1;36m' ;;
    bold_white) color_code='\033[1;37m' ;;
    *) color_code='\033[0m' ;; # default to no color
  esac

  # Escape the color reset code
  reset_code='\033[0m'

  # Use perl to find and replace the regex matches with colored text
  colored_text=$(perl -pe "s/($regex)/${color_code}\$1${reset_code}/g" <<< "$text")

  # Print the colored text
  echo -e "$colored_text"
}
# Example usage
# color_text "Hello, World! This is a test." "World" "red"
# color_text "Here is another example with multiple matches: example, example." "example" "bold_green"

DOCS=$(grep -srn "#@#" | awk '!/cut/' | sed -E 's/([^:]+):([0-9]+):(.*)/\1:00000\2:\3/' | sed -E 's/([^:]+):0*([0-9]{5}):/\1:\2:/' | sort | cut -d"@" -f2- | cut -c 3-)

echo -e "\033[0;35m#################################\033[0m"
echo -e "\033[0;35m#\033[0m \033[0;32mSelf-generating documentation\033[0m \033[0;35m#\033[0m"
echo -e "\033[0;35m#################################\033[0m"
echo ""

# echo "$DOCS"
FDOCS=$(color_text "$DOCS" "^[^ ].+:" "blue")
PDOCS=$(color_text "$FDOCS" "[^*]+ plugin:" "yellow")

echo -e "$PDOCS"

echo -e "\nYou can call it like this for better navigation:"
color_text " $ ./documentation.sh | less -R" ".*" "green"

popd
