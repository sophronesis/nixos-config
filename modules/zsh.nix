{ inputs, config, lib, pkgs, ... }:
{
  #@# modules/zsh.nix: zsh configuration module 
  #@#   contains:
  #@#     options:
  options = {
    nyx.zsh.enable =
      lib.mkEnableOption "enables zsh shell";
    #@#     * nixos config location
    nyx.zsh.flakePath = lib.mkOption {
      type = lib.types.str;
      default = "~/.config/nixos/";
      description = "Path to current flake to be build";
    };
    #@#     * current nixos system ID
    nyx.zsh.flakeNixosID = lib.mkOption {
      type = lib.types.str;
      default = "sphHpNixos";
      description = "ID for current configuration inside flake";
    };
    #@#     * ntfy.sh channel for notifications 
    nyx.zsh.ntfyshChannel = lib.mkOption {
      type = lib.types.str;
      default = "";
      description = "ntfy.sh channel to send notifications to on task completeon";
    };
    #@#     * current zsh prompt theme 
    nyx.zsh.promptTheme = lib.mkOption {
      type = lib.types.str;
      default = "Minimal";
      description = "Theme for zsh prompt. \"Minimal\" and \"Classic\" are available";
    };
    #@#     * station name for ntfy.sh notifications
    nyx.zsh.stationID = lib.mkOption {
      type = lib.types.str;
      default = "NIXOS_DEFAULT";
      description = "Station nickname for ntfy.sh notifications";
    };
    #@#     * station name for prompt
    nyx.zsh.s_ID = lib.mkOption {
      type = lib.types.str;
      default = "X";
      # do you will get something like 
      # sph(X)
      description = "Station id (should be one letter) e.g. H for HP pavilion, F for framework 16 and D for digital ocean";
    };
    nyx.zsh.s_CLR = lib.mkOption {
      type = lib.types.str;
      default = "green";
      # do you will get something like 
      # sph(X)
      description = "Station id color";
    };
  };
  config = lib.mkIf config.nyx.zsh.enable {
    programs.zsh = {
      enable = true;
      #@#     aliases: 
      shellAliases = {
        #whois = "whois -H";
        # rm = "trash_put"; # protecting from random "rm -fr /home" attacks
        # @#     * rm -> warning to use \\rm
        # rm = "echo 'Try prepending \"\\\" to confirm'; false";
        # @#     * yt2mp3 -> download youtube video to mp3
        # yt2mp3 = "youtube-dl -x --prefer-ffmpeg --audio-format mp3";
        #@#     * rootdirbirth -> get root dir creation date
        rootdirbirth = "stat / | awk '/Birth: /{print $2 \" \" substr($3,1,5)}'";
        #@#     * ls -> lsd
        ls = "lsd";
        ll = "lsd -lah";
        tree = "lsd --tree";
        # @#     * nyx-upd -> update system and switch to it
        #nyx-upd = "sudo echo -e '\\c';sudo nixos-rebuild switch --cores 8 --max-jobs 8 --flake ${config.nyx.zsh.flakePath}/#${config.nyx.zsh.flakeNixosID} |& nom";
        # @#     * nyx-cfg -> open configuration folder
        #nyx-cfg = "neovide ${config.nyx.zsh.flakePath}";
        # @#     * nyx-cfg -> clear unused packages older than a week        # 
        #nyx-clr = "nix-collect-garbage --delete-older-than 7d";
        history = "history 1";
        #@#     * v -> neovim
        v = "nvim";
        #@#     * vd -> neovide
        vd = "neovide";
        #@#     * c -> clear terminal
        c = "clear";
        grep = "grep --color=auto";
        # nobrainfog = "ffplay -nodisp -f lavfi -i \"sine=f=3328\"";
      };
      #@#   * autosuggestions, syntax highlighting 
      # TODO transfer to main config:
      autosuggestions.enable = true;
      # histFile = "${home.homeDirectory}/.config/.zsh_history";
      histFile = "$HOME/.config/.zsh_history";
      histSize = 1000000;

      # histFile = "${config.xdg.dataHome}/zsh/history";
      setOptions = [ "INC_APPEND_HISTORY" ];
      syntaxHighlighting.enable = true;

      # all other setting and configuration
      # additional functions etc. (TODO bad path)
      # custom prompt
      interactiveShellInit = ''
        # additional functions etc. (TODO bad path)
        export STATION_ID="${config.nyx.zsh.stationID}"
        export PROMPT_THEME="${config.nyx.zsh.promptTheme}" # "Classic" and "Minimal" available at zsh_prompt file
        export NTFYSH_CHAN="${config.nyx.zsh.ntfyshChannel}"
        source $HOME/.config/zsh/rc
        # eval "$(direnv hook zsh)" # should be conditional on direnv installed
        # This function is called whenever a command is not found.
        command_not_found_handler() {
          local p=${pkgs.comma}/bin/comma
          if [ -x $p ]; then
            # Run the helper program.
            echo "Command "$1" not found. Trying to run it with comma cli tool."
            $p -p "$@"
          else
            # Indicate than there was an error so ZSH falls back to its default handler
            echo "$1: command not found" >&2
            return 127
          fi
        }
        
        # force zsh to reload prompt
        # used only for direnv color update
        # function chpwd() {
        #   source ~/.config/zsh/prompt
        # }
        # https://superuser.com/questions/601480/always-run-a-command-after-another-command
        function _upd_prompt() {
          source ~/.config/zsh/prompt
        }
        add-zsh-hook chpwd _upd_prompt


        # fzf integration
        if command -v fzf-share >/dev/null; then
          source "$(fzf-share)/key-bindings.zsh"
          source "$(fzf-share)/completion.zsh"
        fi
      '';
      promptInit = ''
        # custom prompt
        export S_ID="${config.nyx.zsh.s_ID}"
        export S_CLR="${config.nyx.zsh.s_CLR}"
        source $HOME/.config/zsh/prompt
      '';
    };
    #@#   * autorun of commands not found
    #@#     it will try find given binary name in nix-index using "comma"
    #@#     and will run it without using nix-shell
    programs.command-not-found.enable = false;
    users.defaultUserShell = pkgs.zsh;
  };
  # TODO modularize hm later
  # options = {
  #   # nyx.zsh.enable = 
  #   #   lib.mkEnableOption "enables zsh shell with all fancy configs";
  #   nyx.zsh.ntfyshChannel = lib.mkOption {
  #     type = lib.types.str;
  #     default = "";
  #     description = "ntfy.sh channel to send notifications to on task completeon";
  #   }; 
  #   nyx.zsh.promptTheme = lib.mkOption {
  #     type = lib.types.str;
  #     default = "Minimal";
  #     description = "Theme for zsh prompt. \"Minimal\" and \"Classic\" are available";
  #   }; 
  #   nyx.zsh.stationID = lib.mkOption {
  #     type = lib.types.str;
  #     default = "NIXOS_DEFAULT";
  #     description = "Station nickname for ntfy.sh notifications";
  #   }; 
  #   nyx.zsh.s_ID = lib.mkOption {
  # type = lib.types.str;
  #     default = "X";
  #     description = "Station id for \"Classic\" zsh prompt theme (should be one letter)";
  #   }; 
  # };
  # nyx.zsh = {
  #   # enable = true;
  #   # Channel to send notifications to from zsh
  #   ntfyshChannel = "amogusamogus123321654456";
  #   # Theme for zsh prompt
  #   # Available options: "Minimal", "Classic"
  #   promptTheme = "Minimal";
  #   # Station nickname for ntfysh
  #   stationID = "HP_NIXOS";
  #   # Station id for "Classic" zsh prompt
  #   s_ID = "H";
  # };
  #  interactiveShellInit = ''
  #  # additional functions etc. (TODO bad path)
  #  export STATION_ID="${nyx.zsh.stationID}"
  #  export PROMPT_THEME="${nyx.zsh.promptTheme}" # "Classic" and "Minimal" available at zsh_prompt file
  #  export NTFYSH_CHAN="${nyx.zsh.ntfyshChannel}"
  #  source ${home.homeDirectory}/.config/zsh/rc 
  #  '';
  #  promptInit = ''
  #  # custom prompt
  #  export S_ID="${nyx.zsh.s_ID}" # used in Classic prompt theme
  #  source ${home.homeDirectory}/.config/zsh/prompt
  #  '';
}
