{ inputs, config, lib, pkgs, pkgs-stable, ... }:
{
  #@# modules/virt.nix: virtualisation module 
  #@#   contains:
  options = {
    nyx.virt.enable =
      lib.mkEnableOption "enables docker, waydroid, distrobox and other virtualisation related stuff";
  };
  config = lib.mkIf config.nyx.virt.enable {
    # hardware.nvidia-container-toolkit = {
    #   enable = true;
    #   mount-nvidia-executables = true;
    # };
    #@#   * docker, nvidia-docker
    virtualisation = {
      docker = {
        enable = true;
            # enableNvidia = true;
            rootless = {
              enable = true;
              setSocketVariable = true;
            };
            package = pkgs.docker_25;
      };
      #   # containers.cdi.dynamic.nvidia.enable = true;
      # #@#   * waydroid 
      # waydroid.enable = true;
    };

    # make appimage binaries run as native apps
    #@#   * appimage native integration
    boot.binfmt.registrations.appimage = {
      wrapInterpreterInShell = false;
      interpreter = "${pkgs.appimage-run}/bin/appimage-run";
      recognitionType = "magic";
      offset = 0;
      mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff'';
      magicOrExtension = ''\x7fELF....AI\x02'';
    };

    #@#   * distrobox, docker-compose
    environment.systemPackages = with pkgs; [
      # qemu
      distrobox
      appimage-run
      # nvidia-docker
      # nvidia-container-toolkit
      # docker
      # docker-compose
      # virtualbox
      
      # virt-manager
      # virt-viewer
      # spice
      # spice-gtk
      # spice-protocol
      # win-virtio
      # win-spice
      # gnome.adwaita-icon-theme
    ];


    programs.virt-manager.enable = true;
    users.groups.libvirtd.members = ["sph"];
    virtualisation.libvirtd.enable = true;
    virtualisation.spiceUSBRedirection.enable = true;

    # fixing non-nix compiled binaries
    # TODO maybe should be elsewhere
    #@#   * nix-ld fix for non-nixos binaries 
    # programs.nix-ld.enable = true;
    # programs.nix-ld.libraries = with pkgs; [
    #   # Add any missing dynamic libraries for unpackaged programs
    #   # here, NOT in environment.systemPackages
    # ];

    # KVM
    # virtualisation.libvirtd.enable = true;
    # programs.dconf.enable = true;

    # # from https://www.reddit.com/r/NixOS/comments/177wcyi/best_way_to_run_a_vm_on_nixos/
    # users.users.gcis.extraGroups = [ "libvirtd" ];

    # virtualisation = {
    #   libvirtd = {
    #     enable = true;
    #     qemu = {
    #       swtpm.enable = true;
    #       ovmf.enable = true;
    #       ovmf.packages = [ pkgs.OVMFFull.fd ];
    #     };
    #   };
    #   spiceUSBRedirection.enable = true;
    # };
    # services.spice-vdagentd.enable = true;
    # programs.virt-manager.enable = true;
    # users.extraGroups.vboxusers.members = [ "sph" ];
    # virtualisation.virtualbox = {
    #   host = {
    #     enable = true;
    #     # enableExtensionPack = true;
    #     # package = pkgs-stable.virtualbox;
    #   };
    # };


    # TODO add nix-alien
  };
}
