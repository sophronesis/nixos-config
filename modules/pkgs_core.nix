{ inputs, system, config, lib, pkgs, pkgs-stable, ... }:
{
  #@# modules/pkgs_core.nix: list of applications to be loaded unconditionally
  #@#   contains:
  # apps
  programs = {
    neovim = {
      enable = true;
      defaultEditor = true;
    };
    thunar.enable = true;
  };

  #@#   * fingerprint detection drivers
  services.fprintd = {
    enable = true;
  };

  #@#   * tailscale options
  # src: https://martin.baillie.id/wrote/tailscale-support-for-nixos/
  services.tailscale.enable = true;
  # Optional (default: 41641):
  # services.tailscale.port = 12345;
  # networking.firewall.allowedUDPPorts = [ ${services.tailscale.port} ];
  networking.firewall.allowedUDPPorts = [ 41641 ];

  programs.java.enable = true;
  
  # * Ollama configuration
  services.ollama = {
    enable = true;
    acceleration = "rocm";
    environmentVariables = {
      CC_AMDGPU_TARGET = "gfx1102"; # used to be necessary, but doesn't seem to anymore
    };
    host = "0.0.0.0";
    rocmOverrideGfx = "11.0.2";
    package = pkgs-stable.ollama;
  };

  services.open-webui = {
    enable = true;
  };

  services.flatpak.enable = true;

  # networking.nftables.enable = true;

  # sniffnet's wrappers to get
  security.wrappers = {
    sniffnet = {
      owner = "root";
      group = "root";
      #capabilities = "cap_net_raw,cap_net_admin=eip";
      capabilities = "cap_net_raw,cap_net_admin=eip";
      source = "${pkgs.sniffnet}/bin/sniffnet";
      # source = "/nix/store/4r1l40wm3mrx1s189djbcgx6knrladad-sniffnet-1.3.1/bin/sniffnet";
    };
  };

  environment.systemPackages = with pkgs; [
    #@#   * unsorted packages
    
    #element-desktop-wayland
    inputs.nix-search-tv.packages.x86_64-linux.default
    television
    nix-search
    sniffnet
    fortune
    ripgrep # needed for nvim
    gdu # for nvim
    bottom # for nvim
    aichat
    # screen
    minicom
    scanmem # practicaly game engine
    rustc
    cargo
    # nbfc-linux # temp? # doesnt help on fw16
    # tuxclocker # temp?
    # tcpdump # temp
    # nmap # temp
    # iptables
    # nftables
    #ventoy
    # for steam VR
    procps
    usbutils
    tealdeer # tldr
    ncdu
    # busybox # for lsusb for steamvr
    tailscale
    fastfetch
    # signal-desktop
    # inputs.nix-software-center.packages.${system}.nix-software-center
    upower
    hyprlock
    powertop
    comma # required for command_not_found_handler in zsh
    thunderbird
    # birdtray
    # tor-browser
    # trash-cli
    # cargo # for nil language server used in neovim
    obsidian
    devenv
    # pinta #  paint-like image editor
    translatepy # for translate_loop.sh script
    # mullvad-vpn

    #@#   * general packages 
    # common::console
    wget
    git
    tmux
    fzf
    zip
    p7zip
    killall
    gcc
    gnumake
    jq
    lsd
    fd
    # tldr - alternative to man
    # dtrx - archive extractor
    ffmpeg
    # youtube-dl
    whois
    dig
    bc
    progress
    unzip
    unrar
    tree
    openvpn
    # entr - run command on filesystem changes

    #@#   * tui applications
    neovim
    htop
    btop
    # nvtop - have nvidia drivers dependency
    du-dust # also ncdu, duf, dysk
    # sptlrx - spotify synced lyrics
    # peaclock - clock app
    gping
    # iftop - show network traffic ip-vise. $ sudo iftop -i wlo1
    # nethogs # show network traffic process-vise. $ sudo nethogs -v 3

    #@#   * graphical applications
    firefox
    kitty # terminal emulator
    feh
    neovide # neovim wrapper
    nwg-look # gtk application theming
    telegram-desktop
    libsForQt5.kolourpaint
    libsForQt5.okular
    mpv
    # hardinfo - hardware info
    # ncspot # spotify client
    keepassxc
    # psi-plus
    # typora
    onlyoffice-bin_latest
    popcorntime # film/series direct torrenting
    torrential # torrent client. also deluge
    electrum # bitcoin wallet
    # feather # monero wallet
    # activitywatch - apps usage statistics
    # (pkgs.callPackage ./packages/awatcher/package.nix {}) # still shitty linux support
    # blanket # ambient sounds generator
    # syncthing # synchronization tool

    #@#   * nix/nixos packages 
    cachix # binary cache
    nix-index # search inside all packages, similar to "pacman -F"
    nix-output-monitor # nom, prettifier for output of rebuild
    nvd # diff for two generations
    nix-tree # tui app to inspect nix storage tree
    # manix # man for nix options
    # nix-software-center - gui app, similar to appstore

    #@#   * machine learning applications
    # ollama
    # aichat
    # (pkgs.ollama.override { acceleration = "cuda"; }) - same, but with cuda support
    #(pkgs.ollama.override { acceleration = "rocm"; }) # - same, but with cuda support

    # st terminal emulator with patches:
    # (st.overrideAttrs (oldAttrs: rec {
    #   # ligatures dependency
    #   buildInputs = oldAttrs.buildInputs ++ [ harfbuzz ];
    #   patches = [
    #     # ligatures patch
    #     # (fetchpatch {
    #     #   url = "https://st.suckless.org/patches/ligatures/0.8.3/st-ligatures-20200430-0.8.3.diff";
    #     #   sha256 = "sha256-vKiYU0Va/iSLhhT9IoUHGd62xRD/XtDDjK+08rSm1KE=";
    #     # })
    #     (fetchpatch {
    #       url = "https://st.suckless.org/patches/scrollback/st-scrollback-20210507-4536f46.diff";
    #       sha256 = "sha256-9qzPHaT7Qd03lJfBeFBebvjmJcw8OzVP2nSqLlLr7Pk=";
    #     })
    #     # (fetchpatch {
    #     #   url = "https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20220127-2c5edf2.diff";
    #     #   sha256 = "sha256-CuNJ5FdKmAtEjwbgKeBKPJTdEfJvIdmeSAphbz0u3Uk=";
    #     # })
    #   ];
    #   # version controlled config file
    #   # configFile = writeText "config.def.h" (builtins.readFile "${fetchFromGitHub { owner = "me"; repo = "my-custom-st-stuff"; rev = "1111222233334444"; sha256 = "11111111111111111111111111111111111"; }}/config.h");
    #   # configFile = writeText "config.def.h" (builtins.readFile ./home/sph/.config/st/config.h);
    #   # postPatch = oldAttrs.postPatch ++ ''cp ${configFile} config.def.h'';
    # }))
  ];
}
