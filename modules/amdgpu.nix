{ config, pkgs, pkgs-stable, lib, ... }:
{
  #@# modules/amdgpu.nix: amd drivers settings
  options = {
    nyx.amdgpu.enable = 
      lib.mkEnableOption "enables amd gpu drivers";
  };
  config = lib.mkIf config.nyx.amdgpu.enable {
    hardware.graphics = {
      enable = true;
      enable32Bit = true; # For 32 bit applications
      extraPackages = with pkgs; [
        rocmPackages.clr.icd
      ];
    };  
    boot.initrd.kernelModules = [ "amdgpu" ];
    services.xserver.videoDrivers = [ "amdgpu" ];
    environment.systemPackages = with pkgs; [
      amdgpu_top
      pkgs-stable.rocmPackages_5.rocm-smi
    ];
  };
}
