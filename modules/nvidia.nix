{ config, pkgs, lib, ... }:
{
  #@# modules/nvidia.nix: nvidia drivers settings
  options = {
    nyx.nvidia.enable = 
      lib.mkEnableOption "enables nvidia drivers";
  };
  config = lib.mkIf config.nyx.nvidia.enable {
    # Nvidia configs
    nixpkgs.config.cudaSupport = true;
    hardware.opengl = {
      enable = true;
      # driSupport = true;
      # driSupport32Bit = true;
    };  
    services.xserver.videoDrivers = ["nvidia"];
    hardware.nvidia = {
      modesetting.enable = true;
      # powerManagement.enable = false;
      # powerManagement.finegrained = false;
      open = false;
      nvidiaSettings = true;
      package = config.boot.kernelPackages.nvidiaPackages.stable;
      # package = config.boot.kernelPackages.nvidiaPackages.production;
      # prime = {
      #   offload = {
      #     enable = true;
      #     enableOffloadCmd = true;
      #     # intelBusId and nvidiaBusId should be defined
      #     # in configuration.nix on per machine basis
      #   };
      # };
    };
  };
}
