{ inputs, config, lib, pkgs, ... }:
  #@# modules/pkgs_work.nix: work-related applications
{
  options = {
    nyx.work.enable = 
      lib.mkEnableOption "enables work related packages";
  };

  config = lib.mkIf config.nyx.work.enable {
    environment.systemPackages = with pkgs; [
      google-chrome
      chromium
      # dbeaver # renamed in unstable branch
      #dbeaver-bin
      #kubectl
      #kubernetes
      docker
      #docker-compose
      #nvidia-docker
      #poetry
      #awsli2
      uv

      (python312.withPackages (ps: with ps; [
        numpy
        scipy
        pip
        matplotlib
        #google-api-python-client
        #google-auth-httplib2    
        #google-auth-oauthlib    
        #dasbus                  
        #pygobject3  
        #python-lsp-server
        # ps.bar-gmail
        # poetry
        # working example
        # (python39.pkgs.buildPythonPackage rec {
        #   pname = "findiff";
        #   version = "0.9.2";

        #   src = fetchPypi {
        #     inherit pname version;
        #     sha256 = "sha256-IWqQhGv+8d3mYdnwJh1byj0su2c0K5fpMTylsvR4c2U=";
        #   };

        #   doCheck = false;

        #   buildInputs = [
        #     numpy
        #     scipy
        #     sympy
        #   ];
        # })

        # (python312.pkgs.buildPythonPackage rec {
        #   pname = "bar-gmail";
        #   version = "1.0.4";

        #   # src = fetchPypi {
        #   #   inherit pname version;
        #   #   # sha256 = "sha256-IWqQhGv+8d3mYdnwJh1byj0su2c0K5fpMTylsvR4c2U=";
        #   #   sha256 = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
        #   # };
        #   src = fetchFromGitHub {
        #     owner = "crabvk";
        #     repo = "bar-gmail";
        #     rev = "ca33c40240fbf563c723ff19b3e0feae42f6160a";
        #     sha256 = "0rs9bxxrw4wscf4a8yl776a8g880m5gcm75q06yx2cn3lw2b7v22";
        #   }; 

        #   doCheck = false;

        #   buildInputs = [
        #     google-api-python-client
        #     google-auth-httplib2
        #     google-auth-oauthlib
        #     dasbus
        #     pygobject3
        #     poetry-core
        #   ];
        # })
      ]))
    ];
  };
}
