{ config, lib, pkgs, ... }:
{
  #@# modules/sound.nix: sound and bluetooth settings module
  options = {
    nyx.sound.enable = 
      lib.mkEnableOption "enables sound and bluetooth headphones settings";
  };
  config = lib.mkIf config.nyx.sound.enable {
    # pipewire settings
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    # bluetooth settings
    hardware.bluetooth.enable = true;
    hardware.bluetooth.powerOnBoot = true;
    services.blueman.enable = true;
    
    # removes jitter for connected devices
    # to change in later version
    environment.etc = {
    	"wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
    		bluez_monitor.properties = {
    			["bluez5.enable-sbc-xq"] = true,
    			["bluez5.enable-msbc"] = true,
    			["bluez5.enable-hw-volume"] = true,
    			["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
    		}
    	'';
    };

    environment.systemPackages = with pkgs; [
      pavucontrol # main way to adjust volume
      playerctl   # used to control playback from console/statusbar
      bluetuith
      alsa-utils
      # helvum # gtk gui for pipewire
    ];
  };
}

