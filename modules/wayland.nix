{ inputs, config, lib, pkgs, ... }:
{
  #@# modules/wayland.nix: wayland ecosystem module 
  #@#   contains: 
  options = {
    nyx.wayland.enable = 
      lib.mkEnableOption "enables wayland, hyprland, waybar etc.";
  };
  config = lib.mkIf config.nyx.wayland.enable {
    
    # Enabling hyprland on NixOS
    # TODO add plugins
    #@#   * hyprland, hyprswitch
    programs.hyprland = {
      enable = true;
      # package = inputs.hyprland.packages.${pkgs.system}.hyprland;
      # enableNvidiaPatches = true; # removed in unstable branch
      xwayland.enable = true;
    };
  # };
    
    # without this swaylock won't accept password
    #@#   * swaylock authorization
    security.pam.services.swaylock = {
      text = ''
        auth include login
      '';
    };

    services.dbus.enable = true;
    #@#   * xdg portals
    xdg.portal = {
      enable = true;
      # wlr.enable = true;
      extraPortals = [
        pkgs.xdg-desktop-portal-gtk
      ];
    };
    
    environment.sessionVariables = {
      # If your cursor becomes invisible
      WLR_NO_HARDWARE_CURSORS = "1";
      # Hint electron apps to use wayland
      NIXOS_OZONE_WL = "1";
      # Force software rendering for virtualbox
      WLR_RENDERER_ALLOW_SOFTWARE = "1";
    };

    # sddm login manager
    #@#   * sddm display manager
    # TODO implement smarter theming for sddm
    # https://discourse.nixos.org/t/sddm-background-image/5495
    services = {
      displayManager.sddm = {
        enable = true;
        # theme = "${import  ./../themes/sddm-sugar-dark.nix {inherit pkgs; }}";
        theme = "${import  ./../themes/sddm-aerial.nix {inherit pkgs; }}";
        wayland.enable = true;
      };
    };
    qt = {
      enable = true;
      platformTheme = "gtk2";
    };

    # Fonts
    #@#   * fonts
    fonts.packages = with pkgs; [
      font-awesome
      monocraft
      noto-fonts
      noto-fonts-emoji
      noto-fonts-cjk-sans
      # nerdfonts
      
      # list all nerdfonts packages:
      # $  nix eval nixpkgs#legacyPackages.x86_64-linux.nerd-fonts --apply builtins.attrNames
      nerd-fonts.noto # for kitty
      nerd-fonts.symbols-only # also for kitty

      # nerd-fonts.droid_sans_mono
    ];

    programs.sway = {
      enable = true;
      wrapperFeatures.gtk = true;
    };

    programs.labwc.enable = true;

    environment.systemPackages = with pkgs; [
      hyprland
      # inputs.hyprswitch.packages.x86_64-linux.default
      #@#   * waybar, swww
      # monado
      waybar
      swww
      xdg-desktop-portal
      xdg-desktop-portal-wlr
      xdg-desktop-portal-gtk
      # xdg-desktop-portal-hyprland
      xwayland
      wlr-randr
      wofi
      #@#   * grim, slurp for screenshots
      grim   # for screenshots
      slurp  # for selection screenshot location
      # swaylock
      # swaylock-effects
      # waylock
      #@#   * wlogout logout menu 
      wlogout # logout menu
      wl-clipboard # wayland clipboard
      seatd # don't remember tbh (TODO check)
      #@#   * notifications via swaynotificationcenter
      libnotify # main lib
      swaynotificationcenter # also dunst
      lm_sensors      # needed for waybar "nix" module
      neofetch        # needed for waybar "nix" module
      acpi            # needed for waybar "battery" module
      wttrbar         # needed for waybar "weather" module
      brightnessctl   # needed for hyprland brightness adjustment
      libsForQt5.qt5.qtquickcontrols2   # needed for sddm theme
      libsForQt5.qt5.qtgraphicaleffects # needed for sddm theme
      libsForQt5.qt5.qtmultimedia       # needed for aerial sddm theme
      # mpvpaper - mpv for background video playing (currently swww is enough)

      # fonts
      # TODO check if still needed
      #terminus_font
      #terminus_font_ttf
      #nerdfonts
      #font-awesome
      # monocraft
    ];
  };
}
