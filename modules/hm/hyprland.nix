{ config, inputs, lib, pkgs, ... }:

{
  #@# modules/hm/hyprland.nix: hyprland wm config with plugins
  options = {
    nyx_hm.hyprland.enable = 
      lib.mkEnableOption "enables hyprland wm with plugins";
  };
  config = lib.mkIf config.nyx_hm.hyprland.enable {
    wayland.windowManager.hyprland = {
      enable = true;
      # plugins = [
      #   inputs.Hyprspace.packages.${pkgs.system}.Hyprspace
      # ];
      extraConfig = ''
        # See https://wiki.hyprland.org/Configuring/Monitors/
        #@# config/hypr/hyprland.conf: hyprland config
        #@#   contains:

        # TODO:
        monitor=,preferred,auto,1

        # See https://wiki.hyprland.org/Configuring/Keywords/ for more

        #@#   * apps executed on launch
        exec-once = waybar
        exec-once = blueman-applet
        exec-once = swww query || swww init
        exec-once = swww img /home/sph/.config/nixos/wallpapers/wallpaper_34.jpg
        exec-once = [workspace special:magic silent] thunderbird
        exec-once = [workspace special:magic silent] keepassxc
        exec-once = [workspace special:magic silent] autolaunch_loop

        # Source a file (multi-file configs)
        # source = ~/.config/hypr/myColors.conf
        # %% source paywal (not sure)

        # Some default env vars.
        env = HYPRCURSOR_SIZE,24
        env = XCURSOR_SIZE,24
        # select on which GPU will hyprland will run (for steamvr tests)
        # env = WLR_DRM_DEVICES,/dev/dri/card1:/dev/dri/card2
        # env = WLR_DRM_DEVICES,/dev/dri/card2
        # env = AQ_DRM_DEVICES,/dev/dri/card1,/dev/dri/card2

        # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
        input {
            kb_layout = us,ru,ua
            kb_options=grp:win_space_toggle
            follow_mouse = 2
        }

        general {
            # See https://wiki.hyprland.org/Configuring/Variables/ for more

            gaps_in = 5
            gaps_out = 20
            border_size = 2
            col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
            col.inactive_border = rgba(595959aa)

            layout = dwindle

            # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
            allow_tearing = false
        }

        decoration {
            # See https://wiki.hyprland.org/Configuring/Variables/ for more

            rounding = 10
            active_opacity = 0.97
            inactive_opacity = 0.97
            
            blur {
                enabled = true
                # xray = true
                size = 3
                passes = 1
            }

            #drop_shadow = yes
            #shadow_range = 4
            #shadow_render_power = 3
            #col.shadow = rgba(1a1a1aee)
        }


        animations {
            enabled = yes

            # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

            bezier = myBezier, 0.05, 0.9, 0.1, 1.05

            animation = windows, 1, 7, myBezier
            animation = windowsOut, 1, 7, default, popin 80%
            animation = border, 1, 10, default
            animation = borderangle, 1, 8, default
            animation = fade, 1, 7, default
            animation = workspaces, 1, 6, default
        }


        # Ref https://wiki.hyprland.org/Configuring/Workspace-Rules/
        # "Smart gaps" / "No gaps when only"
        # uncomment all if you wish to use that.
        workspace = w[t1], gapsout:0, gapsin:0
        workspace = w[tg1], gapsout:0, gapsin:0
        workspace = f[1], gapsout:0, gapsin:0
        windowrulev2 = bordersize 0, floating:0, onworkspace:w[t1]
        windowrulev2 = rounding 0, floating:0, onworkspace:w[t1]
        windowrulev2 = bordersize 0, floating:0, onworkspace:w[tg1]
        windowrulev2 = rounding 0, floating:0, onworkspace:w[tg1]
        windowrulev2 = bordersize 0, floating:0, onworkspace:f[1]
        windowrulev2 = rounding 0, floating:0, onworkspace:f[1]

        dwindle {
            # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
            pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
            preserve_split = true # you probably want this
            # no_gaps_when_only = 2
        }

        # master {
        #     # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        #     # new_is_master = true
        # }

        gestures {
            # See https://wiki.hyprland.org/Configuring/Variables/ for more
            workspace_swipe = false
        }

        misc {
            # See https://wiki.hyprland.org/Configuring/Variables/ for more
            disable_hyprland_logo = true
            disable_splash_rendering = true
            force_default_wallpaper = 0 # Set to -1 to enable the anime mascot wallpapers
            mouse_move_enables_dpms = true
            key_press_enables_dpms = true
        }

        cursor {
            no_warps = true
        }

        # Example per-device config
        # See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
        # device:epic-mouse-v1 {
        #     sensitivity = -0.5
        # }

        # Example windowrule v1
        # windowrule = float, ^(kitty)$
        # Example windowrule v2
        # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
        # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
        # windowrulev2 = float, title:^(Picture in picture)$
        # windowrulev2 = pin, title:^(Picture in picture)$ 
        windowrulev2 = opacity 0.5, pinned:1 tag:transp


        # Non modkey binds
        #@#   * screenshot hotkeys
        bind = ,      Print, exec, grim -g "$(slurp -d)" - | wl-copy
        # bind = SHIFT, Print, exec, grim -g "$(slurp -d)"
        bind = CTRL,  Print, exec, grim - | wl-copy

        #@#   * F1-F12 hotkeys
        # F2 - F4
        bind = , XF86AudioLowerVolume , exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
        bind = , XF86AudioRaiseVolume , exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
        # bind = , XF86Display          , exec, notify-send "F4 not yet binded"

        # F6 - F12
        bind = , XF86AudioMute        , exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
        bind = , XF86MonBrightnessDown, exec, brightnessctl set 10%-
        bind = , XF86MonBrightnessUp  , exec, brightnessctl set 10%+
        bind = , XF86AudioPrev        , exec, playerctl previous
        bind = , XF86AudioPlay        , exec, playerctl play-pause
        bind = , XF86AudioNext        , exec, playerctl next
        bind = , XF86RFKill           , exec, notify-send "F12 not yet binded"


        # testing numpad direct key support
        # bind = , code:87              , exec, notify-send "Numpad 01"
        # bind = , code:87              , exec, steam
        # bind = , code:88              , exec, notify-send "Numpad 02"
        # bind = , code:88              , exec, firefox 
        # bind = , code:89              , exec, notify-send "Numpad 03"
        # bind = , code:89              , exec, telegram-desktop 
        # bind = , code:83              , exec, notify-send "Numpad 04"
        # bind = , code:83              , exec, keepassxc 
        # bind = , code:84              , exec, notify-send "Numpad 05"
        # bind = , code:84              , exec, hyprctl kill
        # bind = , code:85              , exec, notify-send "Numpad 06"
        # bind = , code:79              , exec, notify-send "Numpad 07"
        # bind = , code:80              , exec, notify-send "Numpad 08"
        # bind = , code:81              , exec, notify-send "Numpad 09"
        # bind = , code:90              , exec, notify-send "Numpad 00"
        # bind = , code:90              , exec, kitty
        # bind = , code:148             , exec, notify-send "Numpad Calc"
        # bind = , code:106             , exec, notify-send "Numpad /"
        # bind = , code:63              , exec, notify-send "Numpad *"
        # bind = , code:82              , exec, notify-send "Numpad -"
        # bind = , code:86              , exec, notify-send "Numpad +"
        # bind = , code:91              , exec, notify-send "Numpad ."

        # bind = , code:87              , workspace, 1
        # bind = , code:88              , workspace, 2
        # bind = , code:89              , workspace, 3
        # bind = , code:83              , workspace, 4
        # bind = , code:84              , workspace, 5
        # bind = , code:85              , workspace, 6
        # bind = , code:79              , workspace, 7
        # bind = , code:80              , workspace, 8
        # bind = , code:81              , workspace, 9
        # bind = , code:90              , workspace, 10
        # bind = , code:148             , exec, notify-send "Numpad Calc"
        # bind = , code:106             , exec, notify-send "Numpad /"
        # bind = , code:63              , exec, notify-send "Numpad *"
        # bind = , code:82              , exec, notify-send "Numpad -"
        # bind = , code:86              , exec, notify-send "Numpad +"
        # bind = , code:91              , exec, notify-send "Numpad ."

        # See https://wiki.hyprland.org/Configuring/Keywords/ for more
        $mainMod = SUPER

        # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
        #@#   * fast execute hotkeys
        bind = $mainMod, TAB, exec, wofi_window
        bind = $mainMod, RETURN, exec, kitty
        bind = $mainMod, Z, exec, wlogout
        bind = $mainMod, D, exec, toggle_waybar 

        bind = $mainMod SHIFT, Q, exit
        bind = $mainMod SHIFT, X, exec, telegram-desktop
        bind = $mainMod SHIFT, Z, exec, firefox
        bind = $mainMod SHIFT, C, exec, steam_launch
        bind = $mainMod SHIFT, V, exec, sleep 1 && hyprctl dispatch dpms off
       
        # hyprspace config (disabled)
        # bind = $mainMod, A, overview:toggle
        # plugin:overview:showEmptyWorkspace = false
        # plugin:overview:showNewWorkspace = true
        # plugin:overview:exitOnClick = true
        # plugin:overview:panelHeight = 200
        # # plugin:overview:gapsIn = 0
        # # plugin:overview:gapsOut = 0
        # plugin:overview:affectStrut = false

        #@#   * fullscreen modes hotkeys
        bind = $mainMod, F, fullscreen           # all monitor + fs mode
        bind = $mainMod SHIFT, F, fullscreenstate, -1 2 # fs mode only 
        bind = $mainMod CTRL, F, fullscreen, 1   # all monitor only

        #@#   * other hotkeys
        bind = $mainMod, C, killactive, 
        bind = $mainMod, V, togglefloating, 
        bind = $mainMod, E, exec, wofi --show window
        bind = $mainMod, R, exec, wofi --show run
        # bind = $mainMod, P, pseudo, # dwindle
        bind = $mainMod, J, togglesplit, # dwindle

        # Move focus with mainMod + arrow keys
        bind = $mainMod, left, movefocus, l
        bind = $mainMod, right, movefocus, r
        bind = $mainMod, up, movefocus, u
        bind = $mainMod, down, movefocus, d

        # Move window in given direction with mainMod + shift + arrow keys
        bind = $mainMod SHIFT, left, swapwindow, l
        bind = $mainMod SHIFT, right, swapwindow, r
        bind = $mainMod SHIFT, up, swapwindow, u
        bind = $mainMod SHIFT, down, swapwindow, d

        # Move window from one monitor to another
        bind = $mainMod CTRL, up, movewindow, mon:+1
        bind = $mainMod CTRL, down, movewindow, mon:-1

        # Move window to next or previous workspace
        bind = $mainMod CTRL, left, movetoworkspace, -1 # r-1
        bind = $mainMod CTRL, right, movetoworkspace, +1 # r+1

        # Swap two monitors
        # bind = $mainMod, u, swapactiveworkspaces, eDP-1 HDMI-A-2

        # Switch workspaces with mainMod + [0-9]
        bind = $mainMod, 1, workspace, 1
        bind = $mainMod, 2, workspace, 2
        bind = $mainMod, 3, workspace, 3
        bind = $mainMod, 4, workspace, 4
        bind = $mainMod, 5, workspace, 5
        bind = $mainMod, 6, workspace, 6
        bind = $mainMod, 7, workspace, 7
        bind = $mainMod, 8, workspace, 8
        bind = $mainMod, 9, workspace, 9
        bind = $mainMod, 0, workspace, 10

        # Switch workspaces with mainMod + numkeys
        # bind = SUPER, KP_End, workspace, 1
        # bind = SUPER, KP_Down, workspace, 2
        # bind = SUPER, KP_Next, workspace, 3
        # bind = SUPER, KP_Left, workspace, 4
        # bind = SUPER, KP_Begin, workspace, 5
        # bind = SUPER, KP_Right, workspace, 6
        # bind = SUPER, KP_Home, workspace, 7
        # bind = SUPER, KP_Up, workspace, 8
        # bind = SUPER, KP_Prior, workspace, 9
        # bind = SUPER, KP_Insert, workspace, 10
        # bind = , KP_1, exec, notify-send 'kp_1'
        # bind = , KP_2, exec, notify-send 'kp_2' 
        # bind = , KP_3, exec, notify-send 'kp_3' 
        # bind = , KP_4, exec, notify-send 'kp_4' 
        # bind = , KP_5, exec, notify-send 'kp_5' 
        # bind = , KP_6, exec, notify-send 'kp_6' 
        # bind = , KP_7, exec, notify-send 'kp_7' 
        # bind = , KP_8, exec, notify-send 'kp_8' 
        # bind = , KP_9, exec, notify-send 'kp_9' 
        # bind = , KP_0, exec, notify-send 'kp_0' 

        # Move active window to a workspace with mainMod + SHIFT + [0-9]
        bind = $mainMod SHIFT, 1, movetoworkspace, 1
        bind = $mainMod SHIFT, 2, movetoworkspace, 2
        bind = $mainMod SHIFT, 3, movetoworkspace, 3
        bind = $mainMod SHIFT, 4, movetoworkspace, 4
        bind = $mainMod SHIFT, 5, movetoworkspace, 5
        bind = $mainMod SHIFT, 6, movetoworkspace, 6
        bind = $mainMod SHIFT, 7, movetoworkspace, 7
        bind = $mainMod SHIFT, 8, movetoworkspace, 8
        bind = $mainMod SHIFT, 9, movetoworkspace, 9
        bind = $mainMod SHIFT, 0, movetoworkspace, 10

        # Move active window to a workspace without switching workspace with mainMod + CTRL + [0-9]
        bind = $mainMod CTRL, 1, movetoworkspacesilent, 1
        bind = $mainMod CTRL, 2, movetoworkspacesilent, 2
        bind = $mainMod CTRL, 3, movetoworkspacesilent, 3
        bind = $mainMod CTRL, 4, movetoworkspacesilent, 4
        bind = $mainMod CTRL, 5, movetoworkspacesilent, 5
        bind = $mainMod CTRL, 6, movetoworkspacesilent, 6
        bind = $mainMod CTRL, 7, movetoworkspacesilent, 7
        bind = $mainMod CTRL, 8, movetoworkspacesilent, 8
        bind = $mainMod CTRL, 9, movetoworkspacesilent, 9
        bind = $mainMod CTRL, 0, movetoworkspacesilent, 10

        # Pin floating window
        bind = $mainMod CTRL, P, pin
        bind = $mainMod SHIFT, P, tagwindow, transp
        # bind = $mainMod SHIFT, P, toggleopaque

        # Example special workspace (scratchpad)
        bind = $mainMod, S, togglespecialworkspace, magic
        bind = $mainMod SHIFT, S, movetoworkspace, special:magic
        bind = $mainMod CTRL, S, movetoworkspacesilent, special:magic

        # Scroll through existing workspaces with mainMod + scroll
        bind = $mainMod, mouse_down, workspace, e+1
        bind = $mainMod, mouse_up, workspace, e-1

        # Move/resize windows with mainMod + LMB/RMB and dragging
        bindm = $mainMod, mouse:272, movewindow
        bindm = $mainMod, mouse:273, resizewindow
      '';
    };
  };
}

