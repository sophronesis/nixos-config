{ inputs, config, lib, pkgs, modulesPath, ... }:
{
  #@# modules/core.nix: shared parameters for all systems
  #@#   contains: 
  #@#   * nix/nixos settings
  nix.settings.experimental-features = [ "nix-command" "flakes" "ca-derivations" ];
  nixpkgs.config.allowUnfree = true;
  # nixpkgs.config.permittedInsecurePackages = [
  #   "dotnet-sdk-6.0.428"
  # ];

  # Bootloader / Booting process
  #@#   * bootloader settings
  boot = {
    supportedFilesystems = [ "ntfs" ];
    loader = {
      #systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      grub = {
        enable = true;
        device = "nodev";
        efiSupport = true;
        # theme = inputs.nixos-grub-themes.packages.${pkgs.system}.hyperfluent;
        theme = inputs.nixos-grub-themes.packages.${pkgs.system}.nixos;
        #useOSProber = true;
        ### Minegrub:
        # minegrub-theme = {
        #   enable = true;
        #   splash = "100% Flakes!";
        #   # background = "background_options/1.8  - [Classic Minecraft].png";
        #   background = "background_options/1.20 - [Trails & Tales].png";
        #   boot-options-count = 8;
        # };
      };
    };
    # use tmpfs too speedup rebuild (may cause problems for too large updates)
    # TODO check for exact speedup
    tmp.useTmpfs = true;
  };
  #@#   * timezone + locale
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "en_US.UTF-8";

  #@#   * dns list
  networking = {
    networkmanager = {
      enable = true; # Easiest to use and most distros use this by default.
      # insertNameservers = [
      #   "9.9.9.9"
      #   "149.112.112.112" # Quad9
      #   "1.1.1.1"
      #   "1.0.0.1" # Cloudflare 
      #   "8.8.8.8"
      #   "8.8.4.4" # Google 
      # ];
    };
    nameservers = [
      "1.1.1.1"
      "8.8.8.8"
      "9.9.9.9"
    ];
    proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };


  #@#   * cachix servers list (prebuilt binary servers for faster builds)
  # Cachix servers
  nix.settings = {
    substituters = [
      # high priority since it's almost always used
      "https://cache.nixos.org?priority=10"
      "https://anyrun.cachix.org"
      "https://fufexan.cachix.org"
      "https://helix.cachix.org"
      "https://hyprland.cachix.org"
      "https://nix-community.cachix.org"
      "https://nix-gaming.cachix.org"
      "https://cuda-maintainers.cachix.org"
      "https://cache.iog.io"
      "https://nixpkgs-python.cachix.org"
      "https://devenv.cachix.org"
    ];

    trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "anyrun.cachix.org-1:pqBobmOjI7nKlsUMV25u9QHa9btJK65/C8vnO3p346s="
      "fufexan.cachix.org-1:LwCDjCJNJQf5XD2BV+yamQIMZfcKWR9ISIFy5curUsY="
      "helix.cachix.org-1:ejp9KQpR1FBI2onstMQ34yogDm4OgU2ru6lIwPvuCVs="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
      "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
      "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      "nixpkgs-python.cachix.org-1:hxjI7pFxTyuTHn2NkvWCrAUcNZLNS3ZAvfYNuYifcEU="
      "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="
    ];
  };

  # TODO check if this is still relevant
  services = {
    # Configure keymap in X11
    xserver.xkb = {
      layout = "us";
      options = "eurosign:e,caps:escape";
    };
    # printing.enable = true;
    libinput.enable = true; # Enable touchpad support (enabled default in most desktopManager).
    openssh.enable = true;
  };

  # TODO add hashed password 
  #@#   * user settings 
  users.users.sph = {
    isNormalUser = true;
    initialPassword = "sph";
    extraGroups = [
      "wheel"
      "audio"
      "video"
      "docker"
      "networkmanager"
      "libvirtd"
    ];
  };
  nix.settings.trusted-users = [ "sph" "root"];

  # extend life of SSD, disable if on HDD
  services.fstrim.enable = true;

  # slows down build if enabled
  # nix.settings.auto-optimise-store = true;

  # nix store optimisations (hardlinks to same files)
  nix.optimise.automatic = true;
  nix.optimise.dates = [ "04:00" ];

  # nix store garbage collection
  nix.gc = {
    automatic = true;
    dates = "04:30"; # "weekly";
    options = "--delete-older-than 7d";
  };

  ### experiments part:
  # opening ollama ports temporary
  # networking.firewall.enable = false;
  networking.firewall = {
    enable = true;
    #allowedInterfaces = [ "tailscale0" ];
    allowedTCPPorts = [ 11434 ];
    trustedInterfaces = [ "tailscale0" ];
    # allowedTCPPortRanges = [ { from = 11434; to = 11435; } ];
    # allowedUDPPortRanges = [ { from = 11434; to = 11435; } ];
    # allowedTCPPortRanges = [ { from = 11434; to = 11435; } ];
    # allowedUDPPortRanges = [ { from = 11434; to = 11435; } ];
    # interfaces."tailscale0".allowedTCPPorts = [ 11434 ];
    # interfaces."tailscale0".allowedUDPPorts = [ 11434 ];
  }; 


  system.stateVersion = "23.11";
}
