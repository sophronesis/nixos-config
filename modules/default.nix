{ pkg, lib, ... }: {
  #@# modules/default.nix: preloading list of all modules
  imports = [
    ./amdgpu.nix
    ./core.nix
    ./pkgs_core.nix
    ./pkgs_work.nix
    ./sound.nix
    ./nvidia.nix
    ./games.nix
    ./virt.nix
    ./vpn.nix
    ./wayland.nix
    ./zsh.nix
  ];

  # settings to be turn on per workstation
  # nyx.amdgpu.enable = lib.mkDefault false;
  # nyx.work.enable = lib.mkDefault false;
  # nyx.sound.enable = lib.mkDefault false;
  # nyx.nvidia.enable = lib.mkDefault false;
  # nyx.games.enable = lib.mkDefault false;
  # nyx.virt.enable = lib.mkDefault false;
  # nyx.wayland.enable = lib.mkDefault false;
  # nyx.zsh.enable = lib.mkDefault false;
}
