{ inputs, config, lib, pkgs, ... }:
{
  #@# modules/vpn.nix: mullvad vpn module 
  options = {
    nyx.vpn.enable = 
      lib.mkEnableOption "enables mullvad vpn service and updates dns";
  };
  config = lib.mkIf config.nyx.vpn.enable {
    services.mullvad-vpn = {
      enable = true;
      package = pkgs.mullvad-vpn;
    };
    services.resolved = {
      enable = true;
      dnssec = "true";
      domains = [ "~." ];
      fallbackDns = [ "1.1.1.1" "1.0.0.1" "8.8.8.8" ];
      dnsovertls = "true";
    };
  };
}
