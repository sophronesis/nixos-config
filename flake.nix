{
  description = "Sophronesis's main system flake";
  #@# flake.nix: root of this configuration
  #@#   contains:
  #@#   * inputs
  inputs = {
    # temporarry fix for rocm-llvm-libc failing to compile
    # https://www.reddit.com/r/NixOS/comments/1ho6a57/anyone_else_having_issues_with_the_rocmllvmlibc/
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    #nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-unstable";
    # nixpkgs.url = "github:nixos/nixpkgs/585f76290ed66a3fdc5aae0933b73f9fd3dca7e3";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/nur";
    nix-alien = {
      url = "github:thiagokokada/nix-alien";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprswitch = {
      url = "github:h3rmt/hyprswitch/release";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland-qtutils.url = "github:hyprwm/hyprland-qtutils";
    minegrub-theme.url = "github:Lxtharia/minegrub-theme";
    nixos-grub-themes.url = "github:jeslie0/nixos-grub-themes";
    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # framework fans control
    fw-fanctrl = {
      url = "github:TamtamHero/fw-fanctrl/packaging/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-search-tv.url = "github:3timeslazy/nix-search-tv";
    #hyprland = {
    #  type = "git";
    #  url = "https://github.com/hyprwm/Hyprland";
    #  submodules = true;
    #  inputs.nixpkgs.follows = "nixpkgs";
    #};
    #Hyprspace = {
    #  url = "github:KZDKM/Hyprspace";
    #  # Hyprspace uses latest Hyprland. We declare this to keep them in sync.
    #  inputs.hyprland.follows = "hyprland";
    #};
    #nixpkgs-xr.url = "github:nix-community/nixpkgs-xr";

    # textfox.url = "github:adriankarlen/textfox";
    # nix-software-center.url = "github:snowfallorg/nix-software-center";
    # flatpaks.url = "github:GermanBread/declarative-flatpak/stable-v3";
    # hyprland-plugins = {
    #   url = "github:hyprwm/hyprland-plugins";
    #   inputs.hyprland.follows = "hyprland";
    # };
    # Hyprspace = {
    #   url = "github:KZDKM/Hyprspace";
    #   # Hyprspace uses latest Hyprland. We declare this to keep them in sync.
    #   inputs.hyprland.follows = "hyprland";
    # };
  };
  outputs =
    { self
    , nixpkgs
    , nixpkgs-stable
    , home-manager
    , nur
    , nix-alien
    , disko
    # , hyprland
    # , Hyprspace
    , hyprland-qtutils
    , minegrub-theme
    , nixos-grub-themes
    , nix-index-database
    , fw-fanctrl
    , nix-search-tv
    # , nixpkgs-xr
    , ...
    }@inputs:
    let
      system = "x86_64-linux";
    in
    {
      nixosConfigurations = {
        #@#   * sphHpNixos configuration
        sphHpNixos = inputs.nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs system; };
          modules = [
            ./hosts/hp_pavilion/configuration.nix
          ];
        };
        #@#   * sphFW16Nixos configuration
        sphFW16Nixos = inputs.nixpkgs.lib.nixosSystem {
          specialArgs = {
            inherit inputs system; 
            # To use packages from nixpkgs-stable,
            # we configure some parameters for it first
            pkgs-stable = import nixpkgs-stable {
              # Refer to the `system` parameter from
              # the outer scope recursively
              inherit system;
              # To use Chrome, we need to allow the
              # installation of non-free software.
              config.allowUnfree = true;
            };
          };
          modules = [
            fw-fanctrl.nixosModules.default
            ./hosts/framework_16/configuration.nix
          ];
        };
        #@#   * doNixos configuration
        doNixos = inputs.nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs system; };
          modules = [
            ./hosts/do_server/configuration.nix
            # inputs.nix-index-database.nixosModules.nix-index
            # { programs.nix-index-database.comma.enable = true; }
          ];
        };
      };
    };
}
